/*
 * SPDX-License-Identifier: Apache-2.0
 */

export class Transaction {
    public docType?: string;
    public companyInCharge: string;
    public personInCharge: string;
    public productionNo: string;
    public encryptedItems: object;
    public productionTemp?: string;
    public testReport?: string;
    public remark?: string;
    public is_done: boolean;
    public created_time: string;
    public updated_time: string;
}

export class ChannelKey {
    public docType?: string;
    public companyName: string;
    public key: string;
    public partnerCompany: string;
    public created_time: string;
    public updated_time: string;
}

export class PublicKey {
    public docType?: string;
    public companyName: string;
    public publicKey: string;
    public created_time: string;
    public updated_time: string;
}
