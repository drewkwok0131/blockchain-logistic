/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { Context, Contract } from 'fabric-contract-api';
import { Transaction, ChannelKey, PublicKey } from './logistics';

export class FabLogistics extends Contract {

    public async initLedger(ctx: Context) {
        console.info('============= START : Initialize Ledger ===========');
        const initObject: Object[] = [
            {
                docType: 'transaction',
                companyInCharge: 'genesisCom',
                personInCharge: 'genesisStaff',
                productionNo: '000000',
                encryptedItems: {
                    data: '938419uejdwq9dj9'
                },
                productionTemp: '27c',
                testReport: '',
                remark: '',
                is_done: false,
                created_time: new Date().toUTCString(),
                updated_time: new Date().toUTCString()
            },
            {
                docType: 'channelKey',
                companyName: 'genesisCom',
                key: '0x000000000000000000000',
                partnerCompany: 'genesisReceiver',
                created_time: new Date().toUTCString(),
                updated_time: new Date().toUTCString()
            },
            {
                docType: 'publicKey',
                companyName: 'genesisCom',
                publicKey: '0x0000000000000000',
                created_time: new Date().toUTCString(),
                updated_time: new Date().toUTCString()
            }
        ];

        for (let i = 0; i < initObject.length; i++) {
            if (initObject[i]['docType'] === 'transaction') {
                await ctx.stub.putState('transaction1', Buffer.from(JSON.stringify(initObject[i])));
            } else if (initObject[i]['docType'] === 'channelKey') {
                await ctx.stub.putState('channelKey1', Buffer.from(JSON.stringify(initObject[i])));
            } else if (initObject[i]['docType'] === 'publicKey') {
                await ctx.stub.putState('publicKey1', Buffer.from(JSON.stringify(initObject[i])));
            }
            console.info('Added <--> ', initObject[i]);
        }

        console.info('============= END : Initialize Ledger ===========');
    }

    //3 functions that are used to create assets(transaction/channelKey/publicKey) 
    public async createTransaction(
        ctx: Context,
        transactionNumber: string,
        docType: string,
        companyInCharge: string,
        personInCharge: string,
        productionNo: string,
        encryptedItems: object,
        is_done: boolean,
        created_time:string,
        updated_time:string,
        productionTemp?: string | '',
        testReport?: string | '',
        remark?: string | ''
    ) {
        console.info('============= START : Create Transaction ===========');

        const transaction: Transaction = {
            docType,
            companyInCharge,
            personInCharge,
            productionNo,
            encryptedItems,
            is_done,
            created_time,
            updated_time,
            productionTemp,
            testReport,
            remark,

        };

        await ctx.stub.putState(transactionNumber, Buffer.from(JSON.stringify(transaction)));
        console.info('============= END : Create Transaction ===========');
    }


    public async createChannelKey(
        ctx: Context,
        channelKeyNumber: string,
        docType: string,
        companyName:string,
        key: string,
        partnerCompany:string,
        created_time:string,
        updated_time:string
    ) {
        console.info('============= START : Create channel Key ===========');

        const channelKey: ChannelKey = {
            docType,
            companyName,
            key,
            partnerCompany,
            created_time,
            updated_time
        };

        await ctx.stub.putState(channelKeyNumber, Buffer.from(JSON.stringify(channelKey)));
        console.info('============= END : Create channel Key ===========');
    }

    public async createPublicKey(
        ctx: Context,
        publicKeyNumber: string,
        docType: string,
        companyName: string,
        publicKey: string,
        created_time:string,
        updated_time:string
    ) {
        console.info('============= START : Create PublicKey ===========');

        const publicKey_: PublicKey = {
            docType,
            companyName,
            publicKey,
            created_time,
            updated_time
        };

        await ctx.stub.putState(publicKeyNumber, Buffer.from(JSON.stringify(publicKey_)));
        console.info('============= END : Create PublicKey ===========');
    }

    //3 functions that are used to query ledger by using transaction/channelKey/publicKey key
    public async queryTransactionByNumber(ctx: Context, transactionNumber: string): Promise<string> {
        const transactionAsBytes = await ctx.stub.getState(transactionNumber); // get the transaction from chaincode state (format: 'transaction' + number)
        if (!transactionAsBytes || transactionAsBytes.length === 0) {
            throw new Error(`${transactionNumber} does not exist`);
        }
        console.log(transactionAsBytes.toString());
        return transactionAsBytes.toString();
    }

    public async queryChannelKeyByNumber(ctx: Context, channelKeyNumber: string): Promise<string> {
        const channelKeyAsBytes = await ctx.stub.getState(channelKeyNumber); // get the transaction from chaincode state (format: 'transaction' + number)
        if (!channelKeyAsBytes || channelKeyAsBytes.length === 0) {
            throw new Error(`${channelKeyNumber} does not exist`);
        }
        console.log(channelKeyAsBytes.toString());
        return channelKeyAsBytes.toString();
    }

    public async queryPublicKeyByNumber(ctx: Context, publicKeyNumber: string): Promise<string> {
        const publicKeyAsBytes = await ctx.stub.getState(publicKeyNumber); // get the transaction from chaincode state (format: 'transaction' + number)
        if (!publicKeyAsBytes || publicKeyAsBytes.length === 0) {
            throw new Error(`${publicKeyNumber} does not exist`);
        }
        console.log(publicKeyAsBytes.toString());
        return publicKeyAsBytes.toString();
    }


    //query all assets including transaction/channelKey/publicKey
    public async queryAllAsset(ctx: Context): Promise<string> {
        const startKey = '';
        const endKey = '';
        const allResults = [];
        for await (const { key, value } of ctx.stub.getStateByRange(startKey, endKey)) {
            const strValue = Buffer.from(value).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
            } catch (err) {
                console.log(err);
                record = strValue;
            }
            console.info('All query record', record);
            allResults.push({ Key: key, Record: record });
        }
        console.info(allResults);
        return JSON.stringify(allResults);
    }

    // 3 functions that are used to query all transactions/channelKey/publicKey
    public async queryAllTransaction(ctx: Context): Promise<string> {
        const startKey = '';
        const endKey = '';
        const allResults = [];
        for await (const { key, value } of ctx.stub.getStateByRange(startKey, endKey)) {
            const strValue = Buffer.from(value).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
            } catch (err) {
                console.log(err);
                record = strValue;
            }
            if (record.docType === 'transaction') {
                allResults.push({ Key: key, Record: record });
            }
        }
        console.info(allResults);
        return JSON.stringify(allResults);
    }
    
    public async queryAllChannelKey(ctx: Context): Promise<string> {
        const startKey = '';
        const endKey = '';
        const allResults = [];
        for await (const { key, value } of ctx.stub.getStateByRange(startKey, endKey)) {
            const strValue = Buffer.from(value).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
            } catch (err) {
                console.log(err);
                record = strValue;
            }
            if (record.docType === 'channelKey') {
                allResults.push({ Key: key, Record: record });
            }
        }
        console.info(allResults);
        return JSON.stringify(allResults);
    }

    public async queryAllPublicKey(ctx: Context): Promise<string> {
        const startKey = '';
        const endKey = '';
        const allResults = [];
        for await (const { key, value } of ctx.stub.getStateByRange(startKey, endKey)) {
            const strValue = Buffer.from(value).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
            } catch (err) {
                console.log(err);
                record = strValue;
            }
            if (record.docType === 'publicKey') {
                allResults.push({ Key: key, Record: record });
            }
        }
        console.info(allResults);
        return JSON.stringify(allResults);
    }

    public async queryTransactionByStaffName(ctx: Context,staffName: string): Promise<string> {
        const startKey = '';
        const endKey = '';
        const allResults = [];
        for await (const { key, value } of ctx.stub.getStateByRange(startKey, endKey)) {
            const strValue = Buffer.from(value).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
            } catch (err) {
                console.log(err);
                record = strValue;
            }
            if (record.personInCharge === staffName) {
                allResults.push({ Key: key, Record: record });
            }
        }
        console.info(allResults);
        return JSON.stringify(allResults);
    }

    public async queryTransactionByCompanyName(ctx: Context,companyName: string): Promise<string> {
        const startKey = '';
        const endKey = '';
        const allResults = [];
        for await (const { key, value } of ctx.stub.getStateByRange(startKey, endKey)) {
            const strValue = Buffer.from(value).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
            } catch (err) {
                console.log(err);
                record = strValue;
            }
            if (record.companyInCharge === companyName) {
                allResults.push({ Key: key, Record: record });
            }
        }
        console.info(allResults);
        return JSON.stringify(allResults);
    }

    public async queryTransactionByProductionNo(ctx: Context,productionNo: string): Promise<string> {
        const startKey = '';
        const endKey = '';
        const allResults = [];
        for await (const { key, value } of ctx.stub.getStateByRange(startKey, endKey)) {
            const strValue = Buffer.from(value).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
            } catch (err) {
                console.log(err);
                record = strValue;
            }
            if (record.productionNo === productionNo) {
                allResults.push({ Key: key, Record: record });
            }
        }
        console.info(allResults);
        return JSON.stringify(allResults);
    }
}
