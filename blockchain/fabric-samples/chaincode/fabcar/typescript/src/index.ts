/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { FabLogistics } from './fabLogistics';
export { FabLogistics } from './fabLogistics';

export const contracts: any[] = [ FabLogistics ];
