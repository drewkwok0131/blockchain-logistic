# Blockchain Logistic Management System

In order to make use of the benefits of blockchain, such as data decentralization and uncorrupted ledger, this project is aimed to create a mobile application for logistic companies and buyers to record the transaction data through the blockchain system.

## Aim of the project
- Record receipts using blockchain (Hyperledger Fabric in use)
- Each company holds one backend server and database
- Data encryption for confidential data

To know more about the system mechanism, please visit https://bit.ly/3iI9leA

This document is to give instruction for environment setup and how to use the whole system.

## Overview of the folder
The project consist of four main folders:
- **blockchain**, includes the environment setup for Hyperledger Fabric together with CouchDB and the chaincode setup.
- **express**, a node.js backend system written in typescript. It is used to communicate with the system of Hyperledger Fabric and frontend application.
- **logistics-app**, a mobile application using React.js framework.
- **frontendExpress**, a bridge to display logistic-app on the webpage.

## User guideline

## Environment Setup 
Our system is designed for 4 companies to use. We need to bring up the blockchain system and the database one by one.

### Step 1: Bring up 4 hosts in AWS
Please prepare 4 cloud servers and clone the whole project to each server before following the steps. All of the actions below should be run on command line.

### Step 2: Create an overlay with Docker Swarm 

On server 1: 
```
docker swarm init --advertise-addr <server-1 ip address>
docker swarm join-token manager
```

You will see the output of **docker swarm join-token manager**
it will be used for the next step. 

On server 2, 3 and 4,
```
<output from join-token manager> --advertise-addr <host n ip>
```
as n = 2, 3 or 4 base on each server2, 3 and 4

On Host 1, create first-network overlay network,
```
docker network create --attachable --driver overlay first-network
```
After that, all servers are sharing the same overlay.

### Step 3: Bring up each host

Before starting the docker containers, get into the chaincode directory for installing the node modules and building the chaincode, run the following command,

*Note: If you does not setup your chaincode, please go to setup chaincode first. After finish the setup process, you can come back Step 3 for bringing up each host. Refer to [Setup Chaincode](drewkwok0131/blockchain-logistic#chaincode-setup).*

For all servers
```
cd blockchain-logistic/blockchain/fabric-samples/chaincode/fabcar/typescript
npm install
npm run build
```

Then, go to 4host-swarm directory to start docker containers for each host, run the commands below,

```
cd blockchain/fabric-samples/4host-swarm
```

For each server

For server1:
```
./host1up.sh
```

For server2:
```
./host2up.sh
```

For server3:
```
./host3up.sh
```

For server4:
```
./host4up.sh
```

If it is succeed, 5 docker containers with name cli, peer0.org1.example.com, orderer5.example.com, orderer.example.com and couchdb1 will appear in server 1.

4 docker containers will appear in server 2,3,4 respectively, they are:
server2 : cli, peer1.org1.example.com, orderer2.example.com and couchdb2
server3 : cli, peer0.org2.example.com, orderer3.example.com and couchdb3
server4 : cli, peer1.org2.example.com, orderer4.example.com and couchdb4

### Step 4: Bring up mychannel and join all peers to mychannel

On server 1, 
```
./mychannelup.sh
```

After that, all the server have joined mychannel, and ready for chaincode deployment.

### Step 5: Deploy fabcar chaincode

In the deployment of chaincode, we will only execute command line code in server 1.

In server 1,
```
docker exec cli peer lifecycle chaincode package fabcar.tar.gz --path /opt/gopath/src/github.com/chaincode/typescript --lang node --label fabcar_1
```

Then, installing chaincode for each server **(Please be reminded that all command are executing in server 1 command line)**, run the following command
```
#For server 1 (peer0.org1)
docker exec cli peer lifecycle chaincode install fabcar.tar.gz

#For server 2 (peer1.org1)
docker exec -e CORE_PEER_ADDRESS=peer1.org1.example.com:8051 -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer1.org1.example.com/tls/ca.crt cli peer lifecycle chaincode install fabcar.tar.gz

#For server 3 (peer0.org2)
docker exec -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp -e CORE_PEER_ADDRESS=peer0.org2.example.com:9051 -e CORE_PEER_LOCALMSPID="Org2MSP" -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt cli peer lifecycle chaincode install fabcar.tar.gz

#For server 4 (peer1.org2)
docker exec -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp -e CORE_PEER_ADDRESS=peer1.org2.example.com:10051 -e CORE_PEER_LOCALMSPID="Org2MSP" -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer1.org2.example.com/tls/ca.crt cli peer lifecycle chaincode install fabcar.tar.gz
```

After that, we need to approve chaincode for both organizations. And notice that **the value of double-hyphen flag package-id is not the same as the below package id. Please use the output of previous installation.**
```
# for org1
docker exec cli peer lifecycle chaincode approveformyorg --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem --channelID mychannel --name fabcar --version 1 --sequence 1 --waitForEvent --package-id fabcar_1:a976a3f2eb95c19b91322fc939dd37135837e0cfc5d52e4dbc3a2ef881d14179

# for org2
docker exec -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp -e CORE_PEER_ADDRESS=peer0.org2.example.com:9051 -e CORE_PEER_LOCALMSPID="Org2MSP" -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt cli peer lifecycle chaincode approveformyorg --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem --channelID mychannel --name fabcar --version 1 --sequence 1 --waitForEvent --package-id fabcar_1:a976a3f2eb95c19b91322fc939dd37135837e0cfc5d52e4dbc3a2ef881d14179
```

Check approval status,
```
docker exec cli peer lifecycle chaincode checkcommitreadiness --channelID mychannel --name fabcar --version 1 --sequence 1
```

Commit chaincode,
```
docker exec cli peer lifecycle chaincode commit -o orderer.example.com:7050 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem --peerAddresses peer0.org1.example.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses peer0.org2.example.com:9051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt --channelID mychannel --name fabcar --version 1 --sequence 1
```

To check commit status,
```
docker exec cli peer lifecycle chaincode querycommitted --channelID mychannel --name fabcar
```

### Step 6: Invoke initLedger 

After committing the chaincode, the ledger is needed to be invoke by running the following command,
```
docker exec cli peer chaincode invoke -o orderer3.example.com:9050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer3.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n fabcar --peerAddresses peer0.org1.example.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses peer0.org2.example.com:9051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '{"Args":["initLedger"]}'
```

Finally, the chaincode is deployed. You can try the below chaincode search or query methods.

### Step 7 Remove everything

To shut down and remove all the containers for each server. We can use the below command:

```
./hostndown.sh
```
where n <= 4


## Chaincode Setup
There are 13 smart contract methods in our system, they mainly serve as query the ledger or insert new data into ledger, they can divided by 2 main groups:

### Input Data(putState)
```
createTransaction
createChannelKey
createPublicKey
```

### Search or query Data(getState)
```
queryTransactionByNumber
queryChannelKeyByNumber
queryPublicKeyByNumber
queryAllAsset
queryAllTransaction
queryAllChannelKey
queryAllPublicKey
queryTransactionByStaffName
queryTransactionByCompanyName
queryTransactionByProductionNo
```

### InitLedger
Before inputting or searching data from the ledger, we have to initLedger first. Below is the example(details may refer to **blockchain/fabric-sample/chaincode/fabcar/typescript/src/fabLogistics.ts**):

```ts
    public async initLedger(ctx: Context) {
        console.info('============= START : Initialize Ledger ===========');
        const initObject: Object[] = [
            {
                docType: 'channelKey',
                companyName: 'genesisCom',
                key: '0x000000000000000000000',
                partnerCompany: 'genesisReceiver',
                created_time: new Date().toUTCString(),
                updated_time: new Date().toUTCString()
            },
            {
                docType: 'publicKey',
                companyName: 'genesisCom',
                publicKey: '0x0000000000000000',
                created_time: new Date().toUTCString(),
                updated_time: new Date().toUTCString()
            }
        ];

             if (initObject[i]['docType'] === 'channelKey') {
                await ctx.stub.putState('channelKey1', Buffer.from(JSON.stringify(initObject[i])));
            } else if (initObject[i]['docType'] === 'publicKey') {
                await ctx.stub.putState('publicKey1', Buffer.from(JSON.stringify(initObject[i])));
            }
            console.info('Added <--> ', initObject[i]);
        }

        console.info('============= END : Initialize Ledger ===========');
    }

```
Before inserting data into the ledger, we have to create Genesis Block(ie. the first block). Each block can be considered as an object. We have to declare the docType(name of the data, create by your own) and the data we want to store as the object key. After that, use **ctx.stub.putState** method, which is provided by fabric-contract-api, to insert data into the ledger.

To execute initLedger method, please refer to **Step 6 in Environment Setup**.

### Inserting Data and Searching Data
The procedure of inserting data and searching data are straight forward after understanding the logic of initLeger. What we have to do is to create different methods. Here are some examples:

```ts
    public async createChannelKey(
        ctx: Context,
        channelKeyNumber: string,
        docType: string,
        companyName:string,
        key: string,
        partnerCompany:string,
        created_time:string,
        updated_time:string
    ) {
        console.info('============= START : Create channel Key ===========');

        const channelKey: ChannelKey = {
            docType,
            companyName,
            key,
            partnerCompany,
            created_time,
            updated_time
        };

        await ctx.stub.putState(channelKeyNumber, Buffer.from(JSON.stringify(channelKey)));
        console.info('============= END : Create channel Key ===========');
    }
```

```ts
    public async queryTransactionByNumber(ctx: Context, transactionNumber: string): Promise<string> {
        const transactionAsBytes = await ctx.stub.getState(transactionNumber);
        if (!transactionAsBytes || transactionAsBytes.length === 0) {
            throw new Error(`${transactionNumber} does not exist`);
        }
        console.log(transactionAsBytes.toString());
        return transactionAsBytes.toString();
    }

```
Please be aware that putState is to insert data, while getState is to search data.

### Executing Smart Contract

To run the methods of the smart contract, we have to run the below codes on command line(details may refer to **express/recordService.ts**): 

```ts
docker exec cli peer chaincode invoke -o orderer3.example.com:9050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer3.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n fabcar --peerAddresses peer0.org1.example.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses peer0.org2.example.com:9051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '{"function": "createChannelKey","Args":["channelKeyNumber","channelKey","formRecord.companyInCharge","base64Cipher","formRecord.receiver","createTime","updateTime"]}'
```

or user child process to execute:

```ts
import { execSync } from 'child_process'

const exampleJsonData = {
        function: "createChannelKey",
        Args: [
            "channelKey1Logistics1",
            "channelKey",
            "companyInCharge",
            "base64Cipher",
            "receiver",
            "createdTime",
            "updatedTime"
        ]
    }

const exampleJsonStringifyData = JSON.stringify(exampleJsonData)

execSync(`docker exec cli peer chaincode invoke -o orderer3.example.com:9050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer3.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n fabcar --peerAddresses peer0.org1.example.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses peer0.org2.example.com:9051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '${exampleJsonStringifyData}'`).toString('utf8')
```

Pay attention to the last 3 line of the command

```ts
'{"function": "createChannelKey","Args":["channelKeyNumber","channelKey","formRecord.companyInCharge","base64Cipher","formRecord.receiver","createTime","updateTime"]}'
```

where **function** is the docType of the smart contract, **Args** are the data inside. It is necessary to follow on what we have written in fabLogistic.ts strictly. Any missing data will caused error for data inserting.  

## Data Encryption Setup

Both symmetric and asymmetic key are used in this project. Symmetric key is used to encrypt selected transaction items before invoking ledger. Asymmetric key is used to encrypt the symmetric shared by the specific companies.

### Encryption Flow

#### Generating public key and private key
In our design, system will generate a RSA public key and private to register user. Private key will be stored in the database owned by this company user. Public key will be submitted to blockchain for later usage. (Details may refer to **express/userService.ts** from line 72 to 109).

#### Generating channel key
When 2 companies decide to make transaction form through our system, once they finish typing all the required information and click 'send'. System will go through the below logic(details may refer to **express/searchService.ts**:

- Step 1: Check if company owned database has record the channel key to this company, if yes, jump to **Step 4**:
- Step 2: Run queryAllPublicKey to check if this company exist and store her information to  table **partner_company** in database
- Step 3: Create channel key to receiver's company, save it into database, encrypt the channel key using the public key from receiver's company, and submit the encrypted key to blockchain.
- Step 4: Use the channel key to encrypt the selected transaction items, upload data to blockchain. 

After that, form submission will be succeed. Receiver's company will have a notification message with transactionID included. She may search the data by using this transaction ID.








