import { knex } from "./db";
import { UserService } from "./userService";
import { RecordService } from "./recordService";
import { SearchService } from "./searchService";
import { ChainToDBService } from "./chainToDBService";
import { MessageService } from "./messageService";

export let userService = new UserService(knex)
export let recordService = new RecordService(knex)
export let searchService = new SearchService(knex)
export let chainToDBService = new ChainToDBService(knex)
export let messageService = new MessageService(knex)