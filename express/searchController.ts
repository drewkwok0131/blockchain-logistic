import express from "express"
import { wrapControllerMethod } from "./helpers"
import { HttpError } from "./httpError"
import { SearchService } from "./searchService"

export class SearchController {
    router = express.Router()

    constructor(private searchService: SearchService) {
        this.router.post('/search/blockchain', wrapControllerMethod(this.searchBlockchain))
        this.router.post('/search/psql', wrapControllerMethod(this.searchpsql))
    }

    searchBlockchain = async (req: express.Request) => {
        let { method, data } = req.body
        if (!method) throw new HttpError('missing method in request body', 400)
        if (!data) throw new HttpError('missing searchData in request body', 400)

        if (method === 'transactionID') {
            let transactionIDRow = await this.searchService.blockchainByTransactionID(data) 
            console.log('transactionIDRow: ', transactionIDRow);
            
            return transactionIDRow
        }
        if (method === 'productionNo') {
            let productionNoRow = await this.searchService.blockchainByProductionNo(data)
            console.log('productionNoRow: ',productionNoRow);
            return productionNoRow
        }
        if (method === 'personInCharge') {
            let personInChargeRow = await this.searchService.blockchainByPersonInCharge(data)
            console.log('personInChargeRow: ',personInChargeRow );
            return personInChargeRow
        }
        if (method === 'self-history') {
            let companyInCharge = await this.searchService.blockchainByCompanyInCharge(data)
            console.log('CompanyInCharge: ',companyInCharge );
            return companyInCharge
        }
        else{
            return { message: "no transaction record" }
        }
    }

    searchpsql = async (req: express.Request) => {
        let { method, data } = req.body
        if (!method) throw new HttpError('missing method in request body', 400)
        if (!data) throw new HttpError('missing searchData in request body', 400)
        console.log({ method, data });
        
        
        if (method === 'transactionID') {
            let transactionIDRow = await this.searchService.psqlByTransactionID(data) 
            console.log('transactionIDRow: ', transactionIDRow);
            
            return transactionIDRow
        }
        if (method === 'productionNo') {
            let productionNoRow = await this.searchService.psqlByProductionNo(data)
            console.log('productionNoRow: ',productionNoRow);
            return productionNoRow
        }
        if (method === 'personInCharge') {
            let personInChargeRow = await this.searchService.psqlByPersonInCharge(data)
            console.log('personInChargeRow: ',personInChargeRow );
            return personInChargeRow
        }
        if (method === 'self-history'){
            let selfHistoryRow = await this.searchService.psqlByselfHistoryRow(data)
            console.log('selfHistoryRow: ',selfHistoryRow);

            return selfHistoryRow

        }
        else{
            return { message: "no transaction record" }
        }
    }
}
//"transactionID" | "productionNo" | "personInCharge" | "self-history"

