import { Knex } from "knex";
import { TransactionDTO } from "./model";
import forge from 'node-forge'
import { execSync } from "child_process";


export class ChainToDBService {
    constructor(private knex: Knex) { }

    async saveToDB(result: TransactionDTO) {
        //console.log(result);

        let myCompanyID = await this.knex.select('id').from('company').where('username', result.username)
        if (myCompanyID.length === 0) {
            return { message: 'database error in finding own username' }
        }
        if (result.username === result.companyInCharge) {
            return { message: 'this form is created by yourself, no need to save again' }
        }

        let partnerDetails = await this.knex.select('id').from('partner_company').where('name', result.companyInCharge)
        if (partnerDetails.length === 0) {
            //find company details from blockchain, insert to database
            let queryToBlockChain = execSync(`docker exec cli peer chaincode query -n fabcar -C mychannel -c '{"Args":["queryAllPublicKey"]}'`).toString('utf8')
            let companyArray = JSON.parse(queryToBlockChain)
            let targetCompany = (companyArray.filter((company: object) => company['Record']['companyName'] === result.companyInCharge))[0]

            console.log('target company: ', targetCompany);

            let insertPartnerDetails = await this.knex.insert({ name: targetCompany.Record.companyName, public_key: targetCompany.Record.publicKey })
                .from('partner_company').returning('id')
            if (!insertPartnerDetails) return { message: 'failed to insert new partner details to your own database' }
            insertPartnerDetails = [{ id: insertPartnerDetails, public_key: targetCompany.Record.publicKey }]

            partnerDetails = await this.knex.select('id').from('partner_company').where('name', result.companyInCharge)
        }
        console.log('partner_details: ', partnerDetails);

        let sharedKey = await this.knex.select('shared_key').from('channel_key').where('partner_company_id', partnerDetails[0].id)

        if (sharedKey.length === 0) {
            let queryToBlockChain = execSync(`docker exec cli peer chaincode query -n fabcar -C mychannel -c '{"Args":["queryAllChannelKey"]}'`).toString('utf8')
            let channelKeyArray = JSON.parse(queryToBlockChain)
            //console.log('channelKeyArray: ', channelKeyArray);

            let targetChannelKey = (channelKeyArray.filter((company: object) => company['Record']['companyName'] === result.companyInCharge))[0]
            console.log('targetChannelKey: ', targetChannelKey);
            if (!targetChannelKey) {
                return { message: 'cannot find sharedKey' }
            }

            //Decrypt channelKey by own private key
            let myPrivateKey = await this.knex.select('private_key').from('company').where('username', result.username)
            if (myPrivateKey.length === 0) {
                return { message: 'cannot find private key from database' }
            }
            //console.log('myPrivateKey: ', myPrivateKey);

            let privateKeyPem = myPrivateKey[0].private_key
            console.log('privateKeyPem: ', privateKeyPem);

            let rsaPrivateKey = forge.pki.privateKeyFromPem(privateKeyPem)

            let key_iv = rsaPrivateKey.decrypt(
                Buffer.from(targetChannelKey.Record.key, 'base64').toString('binary'),
                'RSA-OAEP'
            )
            console.log('key_iv: ', key_iv);
            
            if (key_iv.length != 64) {
                return { message: 'cannot encrypt this channel key' }
            }
            console.log('key_iv: ', key_iv);

            let key_ = key_iv.slice(0, 32)
            let iv_ = key_iv.slice(32, 64)
            console.log({
                key: key_,
                iv: iv_
            });

            //insert to DB
            console.log({ company_id: myCompanyID, shared_key: { key: key_, iv: iv_ }, partner_company_id: partnerDetails[0].id });
            partnerDetails = await this.knex.insert({ company_id: myCompanyID[0].id, shared_key: { key: key_, iv: iv_ }, partner_company_id: partnerDetails[0].id })
                .from('channel_key').returning('id')
            console.log('partnerDetails[0].id: ', partnerDetails);

        }
        sharedKey = await this.knex.select('shared_key').from('channel_key').where('partner_company_id', partnerDetails[0].id)
        console.log('sharedKey: ', sharedKey);


        console.log();


        let sharedKeyObj = JSON.parse(sharedKey[0].shared_key)
        let key = sharedKeyObj.key
        let iv = sharedKeyObj.iv
        console.log({ key, iv });

        let encryptedData = result.encryptedItems
        console.log('encryptedData: ', encryptedData);


        //try to decrypt data, if failed throw not authorise message
            let decipher = forge.cipher.createDecipher('AES-CBC', key);
            decipher.start({ iv: iv });
            decipher.update(forge.util.createBuffer(Buffer.from(encryptedData, 'base64')))
            decipher.finish()
            // outputs decrypted hex
            let json = decipher.output.getBytes()
            console.log('json: ', json);

            let decryptedResult = JSON.parse(json)
            
            console.log('decryptedResult: ', decryptedResult);            

            let receiver = decryptedResult.receiver
            if (receiver !== result.username) {
                return { message: 'cannot decrypt message by using channel key' }
            }
            let item_amount = decryptedResult.item_amount
            let item_name = decryptedResult.item_name
            let is_done : boolean
            if(result.is_done === 'false'){
                is_done = false
            } else {
                is_done = true
            }
            let personInCharge
            if(result.personInCharge === result.companyInCharge){
                personInCharge = null
            }else{
                personInCharge = result.personInCharge
            }

            let transactionData = {
                company_id : myCompanyID[0].id,
                staff_id : personInCharge,
                partner_company_id : partnerDetails[0].id,
                item_number : result.productionNo,
                item_name : item_name,
                item_amount : item_amount,
                item_temperature : result.productionTemp,
                test_report : result.testReport,
                follow_up : result.remark,
                is_done: is_done
            }
            console.log('transactionData: ', transactionData);

            let checkDB = await this.knex.select('id').into('transaction').where(transactionData)
            console.log('checkDB: ', checkDB);
            if(checkDB.length > 0){
                return {message : 'data is already in database'}
            }        
            //decrypt success, then save to db
            let insertToDB = await this.knex.insert(transactionData).into('transaction').returning('id')
            console.log(insertToDB);
            return {message: 'saved successfully'}

    }

}