import { config } from "dotenv"

config();

if (!process.env.DB_NAME) {
  throw new Error("missing DB_NAME in env")
}
if (!process.env.DB_USERNAME) {
  throw new Error("missing DB_USERNAME in env")
}
if (!process.env.DB_PASSWORD) {
  throw new Error("missing DB_PASSWORD in env")
}
if (!process.env.SESSION_SECRET) {
  throw new Error("missing SESSION_SECRET in env")
}
if (!process.env.JWT_SECRET) {
  throw new Error('missing JWT_SECRET in env')
}

export let env = {
  DB_NAME: process.env.DB_NAME,
  DB_USERNAME: process.env.DB_USERNAME,
  DB_PASSWORD: process.env.DB_PASSWORD,
  SESSION_SECRET: process.env.SESSION_SECRET,
  PORT: +process.env.PORT! || 8100,
  NODE_ENV: process.env.NODE_ENV || "development",
  JWT_SECRET: process.env.JWT_SECRET
}
