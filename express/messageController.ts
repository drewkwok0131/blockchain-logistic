import express from "express";
import { wrapControllerMethod } from "./helpers";
import { HttpError } from "./httpError";
import { MessageService } from "./messageService";
import { GetMessageDTO, MessageDTO } from "./model";

export class MessageController {
    router = express.Router()

    constructor(private messageService: MessageService) {
        this.router.post('/saveMessage', wrapControllerMethod(this.saveMessage))
        this.router.post('/message', wrapControllerMethod(this.getMessage))
        this.router.post('/message/hidden', wrapControllerMethod(this.hiddenMessage))

    }

    saveMessage = async (req: express.Request) => {

        let { message, companyName, transactionNumber } = req.body

        if (!message) throw new HttpError('missing message in request body', 400)
        if (!companyName) throw new HttpError('missing companyInCharge in request body', 400)
        if (!transactionNumber) throw new HttpError('missing transactionNumber in request body', 400)


        let messageDetails: MessageDTO = { message, companyName, transactionNumber }
        let messageResult = await this.messageService.insertMessage(messageDetails)
        console.log(messageResult);

        return messageResult
    }

    getMessage = async (req: express.Request) => {
        let { username, serverSelected } = req.body
        if (!username) throw new HttpError('missing username in request body', 400)
        if (!serverSelected) throw new HttpError('missing serverSelected in request body', 400)

        let getMessageDTO: GetMessageDTO = { username, serverSelected }
        let messageList = await this.messageService.message(getMessageDTO)
        return messageList
    }

    hiddenMessage = async (req: express.Request) => {
        let { id } = req.body
        if (!id) throw new HttpError('missing id in request body', 400)

        let message = await this.messageService.hiddenMessage(id)
        return message
    }
}
