import express from "express";
import { ChainToDBService } from "./chainToDBService";
import { wrapControllerMethod } from "./helpers";
//import { HttpError } from "./httpError";


export class ChainToDBController {
    router = express.Router()

    constructor(private chainToDBService: ChainToDBService) {
        this.router.post('/savetodb', wrapControllerMethod(this.saveToDB))
    }

    saveToDB = async (req: express.Request) =>{
        console.log('req.body: ', req.body);
        
        let {companyInCharge,
            created_time,
            docType,
            encryptedItems,
            id,
            is_done,
            personInCharge,
            productionNo,
            productionTemp,
            remark,
            testReport,
            updated_time,
            username
        } = req.body

        let result = {companyInCharge,
            created_time,
            docType,
            encryptedItems,
            id,
            is_done,
            personInCharge,
            productionNo,
            productionTemp,
            remark,
            testReport,
            updated_time,
            username
        }
        console.log('result: ', result);
        let res = await this.chainToDBService.saveToDB(result)
        console.log('res: ', res);      

        return res
    }

    }

