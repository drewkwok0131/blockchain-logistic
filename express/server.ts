import express from 'express'
import { print } from 'listening-on'
import cors from 'cors'
import {env} from './env'
import { UserController } from './userController'
import { chainToDBService, messageService, recordService, searchService, userService } from './service'
import { RecordController } from './recordController'
import { SearchController } from './searchController'
import { ChainToDBController } from './chainToDBController'
import { MessageController } from './messageController'

let app = express()

app.use(express.json())
app.use(cors())

let userController = new UserController(userService)
let recordController = new RecordController(recordService)
let searchController = new SearchController(searchService)
let chainToDBController = new ChainToDBController(chainToDBService)
let messageController = new MessageController(messageService)

app.use(userController.router)
app.use(recordController.router)
app.use(searchController.router)
app.use(chainToDBController.router)
app.use(messageController.router)

app.listen(env.PORT, () => {
  print(env.PORT)
})

