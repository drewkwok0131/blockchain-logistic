export type RegisterUsersDTO = {
    username: string
    password: string
    email: string
    role: string
    serverSelected: string
    company_belong?: string    
}

export type LoginUserDTO = {
    username: string
    password: string
    role: string
}
export type MessageDTO = {
    message: string
    companyName: string
    transactionNumber:string
}

export type GetMessageDTO = {
    username: string
    serverSelected: string
}



export type AccountInfoDTO = {
    username: string
}

export type Company = {
    id: number
    username: string
    password_hash: string
    email: string
    public_key: string
}

export type Users = {
    id: number
    username: string
    password_hash: string
    email: string
    company_id: number
    is_approved: boolean    
}

export type JWTPayload = {
    id: number
    username: string
    role: string
}

export type formRecordDTO = {
    personInCharge: string
    companyInCharge: string
    receiver: string
    productionNo: string
    productionName: string
    productionQuantity: string
    productionTemp: string
    testReport?: string 
    remark?: string
    is_done: boolean
    serverSelected: string
}

export type changePasswordDTO = {
    username : string
    company : string
    oldPassword : string
    newPassword : string
}


export type TransactionDTO = {
    companyInCharge: string
    created_time: string
    docType: string
    encryptedItems: string
    id: string
    is_done: string
    personInCharge: string
    productionNo: string
    productionTemp: string
    remark: string
    testReport: string
    updated_time: string
    username: string
}