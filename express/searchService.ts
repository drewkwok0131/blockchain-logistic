import { execSync } from "child_process";
import { Knex } from "knex";

export class SearchService {
    constructor(private knex: Knex) { }

    async blockchainByTransactionID(transactionNumber:string) {
        try{
            let queryToBlockChain = execSync(`docker exec cli peer chaincode query -n fabcar -C mychannel -c '{"Args":["queryTransactionByNumber","${transactionNumber}"]}'`).toString('utf8')
            return {result : [{...JSON.parse(queryToBlockChain),id: transactionNumber}]}
        }catch (e){
            console.log(e);
            return { message: "no transaction record" }
        }
    }

    async blockchainByProductionNo(productionNo:string) {
        try{
            let queryToBlockChain = execSync(`docker exec cli peer chaincode query -n fabcar -C mychannel -c '{"Args":["queryTransactionByProductionNo","${productionNo}"]}'`).toString('utf8')
    
            let array = JSON.parse(queryToBlockChain)
    
            let resultList = array.map((item:object) =>{
                return {
                    ...item['Record'],
                    id: item['Key']
                }
            })

            if(resultList.length === 0){
                return { message: "no transaction record" }
            }

            return {result : resultList}
        }catch(e){
            console.log(e);
            return { message: "no transaction record" }
        }
    }

    async blockchainByPersonInCharge(staffName: string) {
        try{
            let queryToBlockChain = execSync(`docker exec cli peer chaincode query -n fabcar -C mychannel -c '{"Args":["queryTransactionByStaffName","${staffName}"]}'`).toString('utf8')
            let array = JSON.parse(queryToBlockChain)
    
            let resultList = array.map((item:object) =>{
                return {
                    ...item['Record'],
                    id: item['Key']
                }
            })
            
            if(resultList.length === 0){
                return { message: "no transaction record" }
            }

            return {result : resultList}
        }catch(e){
            console.log(e);
            return { message: "no transaction record" }
        }
    }

    async blockchainByCompanyInCharge(companyName:string) {
        try{
            let queryToBlockChain = execSync(`docker exec cli peer chaincode query -n fabcar -C mychannel -c '{"Args":["queryTransactionByCompanyName","${companyName}"]}'`).toString('utf8')
            let array = JSON.parse(queryToBlockChain)
    
            let resultList = array.map((item:object) =>{
                return {
                    ...item['Record'],
                    id: item['Key']
                }
            })
            
            if(resultList.length === 0){
                return { message: "no transaction record" }
            }

            return {result : resultList}
        }catch(e){
            console.log(e);
            return { message: "no transaction record" }
        }
    }




    async psqlByTransactionID(id: string) {
        let transaction = await this.knex.select('*')
            .from('transaction')
            .where('id', id)

        if (transaction.length === 0) {
            return { message: "no transaction record" }
        }

        let companyName = await this.knex.select('username')
            .from('company')
            .where('id', transaction[0].company_id)

        let staffName
        try {
            staffName = await this.knex.select('username')
                .from('staff')
                .where('id', transaction[0].staff_id)
            staffName = staffName[0].username
        } catch (error) {
            staffName = ''
        }

        let partnerCompanyName = await this.knex.select('name')
            .from('partner_company')
            .where('id', transaction[0].partner_company_id)

        let result = {
            id: transaction[0].id,
            created_time: transaction[0].created_at.toLocaleString(),
            updated_time: transaction[0].updated_at.toLocaleString(),
            companyInCharge: companyName[0].username,
            personInCharge: staffName,
            receiver: partnerCompanyName[0].name,
            productionNo: transaction[0].item_number,
            productionName: transaction[0].item_name,
            productionQuantity: transaction[0].item_amount,
            productionTemp: transaction[0].item_temperature,
            testReport: transaction[0].test_report || '',
            remark: transaction[0].follow_up || '',
            is_done: transaction[0].is_done
        }
        console.log('result: ', result);
        let resultList = [result]  
        return {result: resultList}        
    }

    async psqlByProductionNo(productionNo: string) {
        let transaction = await this.knex.select('*').from('transaction')
            .where('item_number', productionNo)      

        if (transaction.length === 0) {
            return { message: "no transaction record" }
        }

        let companyName = await this.knex.select('username')
            .from('company')
            .where('id', transaction[0].company_id)

        let staffName
        try {
            staffName = await this.knex.select('username')
                .from('staff')
                .where('id', transaction[0].staff_id)
            staffName = staffName[0].username
        } catch (error) {
            staffName = ''
        }

        let partnerCompanyName = await this.knex.select('name')
            .from('partner_company')
            .where('id', transaction[0].partner_company_id)

        let result = {
            id: transaction[0].id,
            created_time: transaction[0].created_at.toLocaleString(),
            updated_time: transaction[0].updated_at.toLocaleString(),
            companyInCharge: companyName[0].username,
            personInCharge: staffName,
            receiver: partnerCompanyName[0].name,
            productionNo: transaction[0].item_number,
            productionName: transaction[0].item_name,
            productionQuantity: transaction[0].item_amount,
            productionTemp: transaction[0].item_temperature,
            testReport: transaction[0].test_report || '',
            remark: transaction[0].follow_up || '',
            is_done: transaction[0].is_done
        }
        console.log('result: ', {result});        
        let resultList = [result]
        console.log('resultList: ', resultList);  
        return {result: resultList}
    }

    async psqlByselfHistoryRow(companyInCharge: string) {
        let companyID = await this.knex.select('id').from('company')
            .where('username', companyInCharge)

        let transaction = await this.knex.select('*').from('transaction')
            .where('company_id', companyID[0].id)

        if (transaction.length === 0) {
            return { message: "no transaction record" }
        }


        let staffName
        try {
            staffName = await this.knex.select('username')
                .from('staff')
                .where('id', transaction[0].staff_id)
            staffName = staffName[0].username
        } catch (error) {
            staffName = ''
        }

        let partnerCompanyName = await this.knex.select('name')
            .from('partner_company')
            .where('id', transaction[0].partner_company_id)


        let result = []

        for (let i = 0; i < transaction.length; i++) {
            result.push({
                id: transaction[i].id,
                created_time: transaction[i].created_at.toLocaleString(),
                updated_time: transaction[i].updated_at.toLocaleString(),
                companyInCharge: companyInCharge,
                personInCharge: companyInCharge,
                receiver: partnerCompanyName[0].name,
                productionNo: transaction[i].item_number,
                productionName: transaction[i].item_name,
                productionQuantity: transaction[i].item_amount,
                productionTemp: transaction[i].item_temperature,
                testReport: transaction[i].test_report || '',
                remark: transaction[i].follow_up || '',
                is_done: transaction[i].is_done
            })
        }
        console.log(result);
        let json = { result }
        console.log('json: ', json);

        return json

    }

    async psqlByPersonInCharge(personInCharge: string) {
        console.log('personInCharge: ', personInCharge);        
        let staff = await this.knex.select('id').from('staff').where('username', personInCharge)
        let staffID
        console.log('staff: ', staff);
        
        if (staff.length === 0) {
           
            return { message: "no transaction record" }            
        } else {
            staffID = staff[0].id
        }
    

        let transaction = await this.knex.select('*').from('transaction')
            .where('staff_id', staffID)

        if (transaction.length === 0) {
            return { message: "no transaction record" }
        }

        let partnerCompanyName = await this.knex.select('name')
            .from('partner_company')
            .where('id', transaction[0].partner_company_id)

        let companyName = await this.knex.select('username')
            .from('company')
            .where('id', transaction[0].company_id)

        console.log('transaction.length: ', transaction.length);

        let result = []

        for (let i = 0; i < transaction.length; i++) {
            result.push({
                id: transaction[i].id,
                created_time: transaction[i].created_at.toLocaleString(),
                updated_time: transaction[i].updated_at.toLocaleString(),
                companyInCharge: companyName[0].username,
                personInCharge: personInCharge,
                receiver: partnerCompanyName[0].name,
                productionNo: transaction[i].item_number,
                productionName: transaction[i].item_name,
                productionQuantity: transaction[i].item_amount,
                productionTemp: transaction[i].item_temperature,
                testReport: transaction[i].test_report || '',
                remark: transaction[i].follow_up || '',
                is_done: transaction[i].is_done
            })
        }
        console.log(result);

        return { result }

    }
}
