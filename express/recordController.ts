import express from "express";
import { wrapControllerMethod } from "./helpers";
import { HttpError } from "./httpError";
import { formRecordDTO } from "./model";
import { RecordService } from "./recordService";

export class RecordController {
    router = express.Router()

    constructor(private recordService: RecordService) {
        this.router.post('/record', wrapControllerMethod(this.recordForm))
    }

    recordForm = async (req: express.Request) => {
        let { personInCharge, companyInCharge, receiver, productionNo, productionName, productionQuantity, productionTemp, testReport, remark, is_done, serverSelected } = req.body
        if (!companyInCharge) throw new HttpError('missing companyInCharge in request body', 400)
        if (!receiver) throw new HttpError('missing receiver in request body', 400)
        if (!productionNo) throw new HttpError('missing productionNo in request body', 400)
        if (!productionName) throw new HttpError('missing productionName in request body', 400)
        if (!productionQuantity) throw new HttpError('missing productionQuantity in request body', 400)

        let formRecord: formRecordDTO = { personInCharge, companyInCharge, receiver, productionNo, productionName, productionQuantity, productionTemp, testReport, remark, is_done, serverSelected }
        let recordResult = await this.recordService.record(formRecord)
        console.log(recordResult);

        return { recordResult }
    }
}

