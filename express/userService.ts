import { Knex } from "knex";
import { comparePassword, hashPassword } from "./hash";
import { HttpError } from "./httpError";
import { AccountInfoDTO, changePasswordDTO, JWTPayload, LoginUserDTO, RegisterUsersDTO } from "./model";
import jwt from 'jwt-simple'
import { jwtConfig } from "./jwt";
import forge from 'node-forge'
import { execSync } from "child_process";

export class UserService {
    constructor(private knex: Knex) { }

    createToken(user: { id: number, username: string, role: string }) {
        let payload: JWTPayload = { id: user.id, username: user.username, role: user.role }
        let token = jwt.encode(payload, jwtConfig.jwtSecret)
        return token
    }

    decodeToken(token: string) {
        try {
            let payload: JWTPayload = jwt.decode(token, jwtConfig.jwtSecret)
            return payload
        } catch (error) {
            throw new HttpError('Invalid JWT token', 401)
        }
    }

    async registerAcc(user: RegisterUsersDTO) {
        let time = (new Date()).toISOString()
        if (user.role == 'staff') {
            let companyID = await this.knex.select('id').from('company').where('username', user.company_belong)
            //console.log(companyID[0]);          
            if (!companyID[0]) {
                throw new HttpError('cannot find this company', 401)
            }
            let checkDuplicatedUser = await this.knex.select('id').from('staff').where('username', user.username)
            //console.log(checkDuplicatedUser[0]);            
            if (checkDuplicatedUser[0]) {
                throw new HttpError('duplicated username', 401)
            }
            let checkDuplicatedEmail = await this.knex.select('id').from('staff').where('email', user.email)
            //console.log(checkDuplicatedEmail[0]); 
            if (checkDuplicatedEmail[0]) {
                throw new HttpError('duplicated email', 401)
            }
            let rows: number = await this.knex
                .insert({
                    username: user.username,
                    password_hash: await hashPassword(user.password),
                    email: user.email,
                    company_id: companyID[0].id
                }).into('staff')
                .returning('id')
            let id = rows[0].id as number
            let token: string = this.createToken({ id, username: user.username, role: 'staff' })
            return token
        }
        if (user.role == 'company') {
            let checkDuplicatedUser = await this.knex.select('id').from('company').where('username', user.username)
            //console.log(checkDuplicatedUser[0]);            
            if (checkDuplicatedUser[0]) {
                throw new HttpError('duplicated username', 401)
            }
            let checkDuplicatedEmail = await this.knex.select('id').from('company').where('email', user.email)
            //console.log(checkDuplicatedEmail[0]); 
            if (checkDuplicatedEmail[0]) {
                throw new HttpError('duplicated email', 401)
            }
            console.log('user: ', user);

            //Generate Public and Private Key, save to Database            
            let pair = forge.pki.rsa.generateKeyPair({ bits: 2048, e: 0x10001 })
            let privateKeyPem = forge.pki.privateKeyToPem(pair.privateKey)
            let publicKeyPem = forge.pki.publicKeyToPem(pair.publicKey)
            console.log({ privateKey: privateKeyPem.length, publicKey: publicKeyPem.length });


            let rows: number = await this.knex.insert({
                username: user.username,
                password_hash: await hashPassword(user.password),
                email: user.email,
                public_key: publicKeyPem,
                private_key: privateKeyPem
            }).into('company')
                .returning('id')

            console.log('rows: ', rows);

            let id = rows[0].id as number
            let token: string = this.createToken({ id, username: user.username, role: 'company' })


            // submit public key to blockchain
            let publicKeyNumber = `publicKey${id + user.serverSelected}`
            let data = {
                function: "createPublicKey",
                Args: [
                    publicKeyNumber,
                    'publicKey',
                    user.username,
                    publicKeyPem,
                    time,
                    time
                ]
            }
            let jsonData = JSON.stringify(data)

            let submitToBlockChain = execSync(`docker exec cli peer chaincode invoke -o orderer3.example.com:9050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer3.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n fabcar --peerAddresses peer0.org1.example.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses peer0.org2.example.com:9051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '${jsonData}'`).toString('utf8')
            console.log(submitToBlockChain);
            
            return token
        } else {
            return 'fail to register'
        }
    }

    async loginAcc(user: LoginUserDTO) {

        if (user.role == 'staff') {
            let row = await this.knex.select('id', 'username', 'password_hash')
                .from('staff')
                .where('username', user.username)
            if (!row[0]) {
                throw new HttpError((`username not found`), 401)
            }
            let isMatch = await comparePassword({
                password: user.password,
                password_hash: row[0].password_hash
            })
            if (!isMatch) {
                throw new HttpError('Wrong password', 401)
            }
            let token: string = this.createToken({ id:row[0].id, username:row[0].username,role:'staff' })
            return token
        }
        if (user.role == 'company') {
            let row = await this.knex.select('id', 'username', 'password_hash')
                .from('company')
                .where('username', user.username)
            if (!row[0]) {
                throw new HttpError((`username not found`), 401)
            }
            let isMatch = await comparePassword({
                password: user.password,
                password_hash: row[0].password_hash
            })
            if (!isMatch) {
                throw new HttpError('Wrong username or password', 401)
            }
            let token: string = this.createToken({ id:row[0].id, username:row[0].username,role:'company' })
            return token
        }
        else {
            return 'failed to login'
        }
    }

    async accountInfo(user: AccountInfoDTO) {
        let row = await this.knex.select('company_id', 'email')
            .from('staff')
            .where('username', user.username)
        if (row.length === 0) {
            let companyUser = await this.knex.select('username', 'email')
                .from('company')
                .where('username', user.username)

            return {
                email: companyUser[0].email,
                companyName: companyUser[0].username
            }
        }
        let companyName = await this.knex.select('username')
            .from('company')
            .where('id', row[0].company_id)

        return {
            email: row[0].email,
            companyName: companyName[0].username
        }
    }

    async changePW(user: changePasswordDTO) {

        if (user.username == user.company) {
            let originalPW = await this.knex.select('password_hash')
                .from('company')
                .where('username', user.username)
            if (!originalPW[0]) {
                throw new HttpError((`username not found`), 500)
            }

            let isMatch = await comparePassword({
                password: user.oldPassword,
                password_hash: originalPW[0].password_hash
            })
            if (!isMatch) {
                throw new HttpError('Wrong username or password', 401)
            }
            await this.knex('company').update({
                password_hash: await hashPassword(user.newPassword)
            }).where('username', user.username).returning('id')

            return { message: 'password updates' }
        } else {
            let originalPW = await this.knex.select('password_hash')
                .from('staff')
                .where('username', user.username)
            if (!originalPW[0]) {
                throw new HttpError((`username not found`), 500)
            }

            let isMatch = await comparePassword({
                password: user.oldPassword,
                password_hash: originalPW[0].password_hash
            })
            if (!isMatch) {
                throw new HttpError('Wrong username or password', 401)
            }
            await this.knex('staff').update({
                password_hash: await hashPassword(user.newPassword)
            }).where('username', user.username).returning('id')

            return { message: 'password updates' }

        }






    }


}

