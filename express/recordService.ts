import { Knex } from "knex";
import forge from 'node-forge'
import { execSync } from 'child_process'
const fetch = require('node-fetch')

export class RecordService {
    constructor(private knex: Knex) { }

    async record(formRecord: {
        personInCharge?: string
        companyInCharge: string
        receiver: string
        productionNo: string
        productionName: string
        productionQuantity: string
        productionTemp: string
        testReport?: string
        remark?: string
        is_done: boolean
        serverSelected: string
    }) {
        let time = (new Date()).toISOString()
        let partnerDetails = await this.knex.select('id', 'public_key', 'ip').from('partner_company').where('name', formRecord.receiver)
        //    console.log('partnerDetails[0]: ', partnerDetails[0]);

        // if there is no existing partner company in psql, server will query the blockchain for public key and the company name. Then, the key and the name will be inserted into private psql database 
        let serverName;
        try {
            if (!partnerDetails[0]) {
                let queryToBlockChain = execSync(`docker exec cli peer chaincode query -n fabcar -C mychannel -c '{"Args":["queryAllPublicKey"]}'`).toString('utf8')
                let companyArray = JSON.parse(queryToBlockChain)
                let targetCompany = (companyArray.filter((company: object) => company['Record']['companyName'] === formRecord.receiver))[0]
                console.log(targetCompany);

                partnerDetails = await this.knex.insert({ name: targetCompany.Record.companyName, public_key: targetCompany.Record.publicKey })
                    .from('partner_company').returning('id')
                let partnerBlockchainID = targetCompany.Key

                let partnerServerName = partnerBlockchainID.substr(partnerBlockchainID.length - 10)
                serverName = partnerServerName
                partnerDetails = [{ id: partnerDetails[0].id, public_key: targetCompany.Record.publicKey, partner_serverName: partnerServerName, partner_companyName: targetCompany.Record.companyName }]
                console.log('partner_details:', partnerDetails[0]);

            }

            let queryBlockChainForMoreDetails = execSync(`docker exec cli peer chaincode query -n fabcar -C mychannel -c '{"Args":["queryAllPublicKey"]}'`).toString('utf8')
            let companyArray = JSON.parse(queryBlockChainForMoreDetails)
            let targetCompany = (companyArray.filter((company: object) => company['Record']['companyName'] === formRecord.receiver))[0]
            let partnerBlockchainID = targetCompany.Key
            let partnerServerName = partnerBlockchainID.substr(partnerBlockchainID.length - 10)
            serverName = partnerServerName



        } catch (e) {
            console.log(e)
        }


        let companyID = await this.knex.select('id').from('company').where('username', formRecord.companyInCharge)
        //    console.log('companyID[0]: ', companyID[0]);        
        let staffRow
        let staffID
        if (formRecord.personInCharge !== formRecord.companyInCharge) {
            staffRow = await this.knex.select('id').from('staff').where('username', formRecord.personInCharge)
            staffID = staffRow[0].id
        } else {
            staffID
        }
        console.log(staffID);


        let channelDetails = await this.knex.select('shared_key', 'company_id').from('channel_key').where('partner_company_id', partnerDetails[0].id)
        console.log('Have channel ID: ????', channelDetails);


        if (!channelDetails[0]) {
            //create shared_key
            let key = forge.random.getBytesSync(16);
            key = forge.util.bytesToHex(key)
            let iv = forge.random.getBytesSync(16);
            iv = forge.util.bytesToHex(iv)

            let sharedKey = { key, iv }
            let saveKeyToDB = await this.knex.insert({ partner_company_id: partnerDetails[0].id, company_id: companyID[0].id, shared_key: sharedKey }).from('channel_key').returning('id')
            console.log("saveKeyToDB: ", saveKeyToDB);
            let channelKeyNumber = `channelKey${saveKeyToDB[0]['id'] + formRecord.serverSelected}`
            // send encrypted shareKey to blockchain
            try {


                let key_iv = key + iv
                console.log('partnerDetails[0].public_key: ', partnerDetails[0].public_key);

                let rsaPublicKey = forge.pki.publicKeyFromPem(partnerDetails[0].public_key)
                console.log('rsaPublicKey:', rsaPublicKey);

                let shareKeyCipher = rsaPublicKey.encrypt(key_iv, 'RSA-OAEP')
                console.log('shareKeyCipher:', shareKeyCipher);

                let base64Cipher = Buffer.from(shareKeyCipher, 'binary').toString('base64')
                console.log("base64Cipher: ", base64Cipher);

                let data = {
                    function: "createChannelKey",
                    Args: [
                        channelKeyNumber,
                        "channelKey",
                        formRecord.companyInCharge,
                        base64Cipher,
                        formRecord.receiver,
                        time,
                        time
                    ]
                }
                let jsonData = JSON.stringify(data)

                let submitToBlockChain = execSync(`docker exec cli peer chaincode invoke -o orderer3.example.com:9050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer3.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n fabcar --peerAddresses peer0.org1.example.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses peer0.org2.example.com:9051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '${jsonData}'`).toString('utf8')

                console.log('shareKey: ', submitToBlockChain);
            } catch (e) {
                console.log(e);
            }
        }


        channelDetails = await this.knex.select('shared_key', 'company_id').from('channel_key').where('partner_company_id', partnerDetails[0].id)

        let sharedKey = JSON.parse(channelDetails[0].shared_key)
        let channelKey = sharedKey.key
        let channeliv = sharedKey.iv
        console.log('shareKey: ', sharedKey);

        let encryptedItems = JSON.stringify({
            receiver: formRecord.receiver,
            item_amount: formRecord.productionQuantity,
            item_name: formRecord.productionName
        })

        let cipher = forge.cipher.createCipher('AES-CBC', channelKey);
        cipher.start({ iv: channeliv });
        cipher.update(forge.util.createBuffer(encryptedItems));
        cipher.finish();
        let encrypted = cipher.output;
        let base64EncryptedData = Buffer.from(encrypted.getBytes(), 'binary').toString('base64')

        console.log('encrypted: ', encrypted);

        //checking encryption result by decryption(debug use only)
        let decipher = forge.cipher.createDecipher('AES-CBC', channelKey);
        decipher.start({ iv: channeliv });
        decipher.update(encrypted);
        console.log('encrypted: ', encrypted);
        console.log('decryption result check: ', decipher.output.data);
        console.log('staffID', staffID);


        //save transaction data to SQL database

        try {
            let recordToOwnDB = await this.knex.insert({
                company_id: companyID[0].id,
                staff_id: staffID,
                partner_company_id: partnerDetails[0].id,
                item_number: formRecord.productionNo,
                item_name: formRecord.productionName,
                item_amount: formRecord.productionQuantity,
                item_temperature: formRecord.productionTemp,
                test_report: formRecord.testReport,
                follow_up: formRecord.remark,
                is_done: false
            }).from('transaction').returning('id')
            console.log('transaction reference ID: ', recordToOwnDB);
            let transactionID = recordToOwnDB[0]
            let blockchainTransactionID = recordToOwnDB[0]['id']

            // the key of the transactionNumber should be `transaction + ${transactionID} + ${serverSelected}`

            let transactionNumber = `transaction${blockchainTransactionID + formRecord.serverSelected}`

            // encrypted data


            let stringifyData = JSON.stringify(base64EncryptedData)
            console.log('stringDATA?!!?!?:',stringifyData);
            
            let stringIsDone = `${formRecord.is_done}`

            let data = {
                function: "createTransaction",
                Args: [
                    transactionNumber,
                    "transaction",
                    formRecord.companyInCharge,
                    formRecord.personInCharge,
                    formRecord.productionNo,
                    base64EncryptedData,
                    stringIsDone,
                    time,
                    time,
                    formRecord.productionTemp,
                    formRecord.testReport,
                    formRecord.remark
                ]
            }
            let jsonData = JSON.stringify(data)
            console.log('print out json string', jsonData);

            let submitToBlockChain = execSync(`docker exec cli peer chaincode invoke -o orderer3.example.com:9050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer3.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n fabcar --peerAddresses peer0.org1.example.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses peer0.org2.example.com:9051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '${jsonData}'`).toString('utf8')
            console.log(submitToBlockChain);

            let messageJSON = {
                message: `${formRecord.companyInCharge} 送了一張收貨紀錄表給你, 可以用以下ID在區塊鏈搜尋該表: ${transactionNumber}`,
                transactionNumber: transactionNumber,
                companyName: formRecord.receiver
            }



            let fetchURL;
            if (serverName === 'logistics1') {
                fetchURL = 'http://13.215.40.77:8080/saveMessage'
            }
            if (serverName === 'logistics2') {
                fetchURL = 'http://13.215.81.97:8080/saveMessage'
            }
            if (serverName === 'logistics3') {
                fetchURL = 'http://52.74.98.243:8080/saveMessage'
            }
            if (serverName === 'logistics4') {
                fetchURL = 'http://54.151.142.38:8080/saveMessage'
            }
            console.log('serverName', serverName);


            console.log('fetch URL is ', fetchURL);
            console.log('messageJSON: ', messageJSON);

            let sendMessageToPartner = await fetch(fetchURL, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(messageJSON)
            })
            console.log(await sendMessageToPartner.json());

            return { message: 'record received', transactionID }

        } catch (error) {
            return { message: 'failed to load database' }
        }
    }
} 
