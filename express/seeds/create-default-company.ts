import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    await knex.insert([
        {
            name: "logistics1", public_key: `-----BEGIN PUBLIC KEY-----
        MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwQ1WVeij6bGmhMgy7gqQ
        lrmNL7IIZQwXmWakFrhcxeXRfIufDUtyku1HCK0WKLokac/upW+nzBfkTE2JdiVW
        YulkZjevAEJqXUSvwcV4kUP+627de0to5z66dBbvBTk3nfWJRXxpKFr+jiUUmltj
        d+HM09lO2aB1aPG+ANCWG9Q+R183GIixRYygzZXn0HQxEwCpF/bre+yO9Ge02qWJ
        a+zoaN5udxnFcLabtbPEgQpB0ViCneNIdUWkdu2Vc6FmNbweTRwfny1gqW8N39qG
        uYckVFiQq6mjXXLHqQnQVf3ESkJZR8J7PQJFQbhHb37/FFd7uotj35xGsj/WPo8z
        dwIDAQAB
        -----END PUBLIC KEY-----`, ip: "http://13.214.233.170:8080"
        },
        {
            name: "logistics2", public_key: `-----BEGIN PUBLIC KEY-----
        MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1lJVu3K7AHr97Jxdy/RW
        dz86UDvQd1uV045TtNqOiYFOzRPScvKNt6O2QLKksjolAEJYv38qbTiWP3FQ8Rx7
        N1+WKhzXFfESZZSNKJxax1rwmh8RNRg23uFeoaGMYt2hrMkHRIIpoO68PeJpw9/J
        m6US2VVii6rMlqDXTRKtN5GhYHMxQLcbrn9KaY80AGXwu8DvAQhBRwGNh0WvBsOz
        xnE9smjGXSC1EqWM0u3LGym7FLlN4GZkG/o8BHePrAj7+w+PT+7jcU+uEgRp1yOi
        1DPhM7CM7In0BcMAiyWZgDznglfaFDOd9G68LSOIUUHiVuDS2iZWX4Obb0FteYjX
        ewIDAQAB
        -----END PUBLIC KEY-----`, ip: "http://13.251.218.142:8080"
        },
        {
            name: "logistics3", public_key: `-----BEGIN PUBLIC KEY-----
        MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlxf643/O6Cs08lQVOBCv
        4Za93mxqjp4kiHL78lbXu+tBdJepxrAgpW7ttJ3zggwF2/CvcYyfBuSR4wUU17/v
        7QPXcRHjyHlkq8qHHql42ztOqq2RZ/o7UtfnXDAWrSVK0mh75LMFIUT314T3yNhk
        dgn2zONNJfMR2UFbD+MdOhjRncpaop4bgwYpUnSs4UUc+rRbvzMQfNlLmMJBOARm
        4r8jQ5fcVL6RBeoD9TEGdjTx/tIyqlhWjoKzXeJnquPFcHlU4cp0kJZrydDyQekl
        47X1eEuZQyUJ1ShaqwHZd43/pr1Tua9lDl+xaQaVAZjH6g+Vx3SO7NREkvm/Q4d4
        EwIDAQAB
        -----END PUBLIC KEY-----`, ip: "http://18.136.229.206:8080"
        },
        {
            name: "logistics4", public_key: `-----BEGIN PUBLIC KEY-----
        MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsBYRfAQWQHS+nk2A1yH1
        p5JFm7Jj2RcXPG5b1nxVZzINZRiv6q1VicLotw1qQB2Hgr9bhDWQPVoXm5D6MjKf
        H0yzWvweNHXn1YbcZ5cCPhm1fmwkBSxpCS4o4hxJ1TFDDywrZUpH0nZEB9oSclHH
        Rx1b2H5iyURCe83QjmWaTKa5d2fis+OcLV8Ixz3cj71ImwWMqk47DeEDf4OYMbeN
        NrNtv2zdrtk5lFS6Ce04P3v/Bxf1paq0B3rnPhmK9rT3xbs5MnUVd5iEnFtgAEce
        BVpDukdKGLuB+3o/wstgeQHZyrELjaVNcykeiSmyab1kUtYY3aESZow9++Imw970
        OQIDAQAB
        -----END PUBLIC KEY-----`, ip: "http://52.221.49.4:8080"
        },
    ]).into('partner_company');
};