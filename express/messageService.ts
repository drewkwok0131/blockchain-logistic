import { Knex } from "knex";

export class MessageService {
    constructor(private knex: Knex) { }

    async insertMessage(message: {
        message: string,
        companyName: string,
        transactionNumber: string
    }) {
        try {
            console.log('getting into the messageService:', message.companyName);

            let companyIDRow = await this.knex.select('id')
                .from('company')
                .where('username', message.companyName)

            console.log(companyIDRow);

            await this.knex.insert({ text: message.message, transactionNumber: message.transactionNumber, company_id: companyIDRow[0].id })
                .from('message')

            return { message: 'inserted message into db' }
        } catch (error) {
            console.log(error);
            return { message: 'error in inserting into db' }
        }
    }

    async message(accountInfo: {
        username: string,
        serverSelected: string
    }) {
        try {
            let companyIDRow = await this.knex.select('id')
                .from('company')
                .where('username', accountInfo.username)

            console.log(companyIDRow);

            let messageList = await this.knex.select('id', 'transactionNumber', 'text', 'is_show')
                .from('message')
                .where('company_id', companyIDRow[0].id)

            console.log(messageList);


            return messageList

        } catch (error) {
            console.log(error);
            return { message: 'fail to query message in db' }
        }
    }

    async hiddenMessage(id: string) {
        try {
            await this.knex('message').where('id', id).update({
                is_show: false
            })
            return { message: 'success to hide message' }

        } catch (error) {
            console.log(error);
            return { message: 'fail to hide message' }
        }
    }
} 
