import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable('message')) return
    await knex.schema.createTable('message', table =>{
        table.increments('id')
        table.string('text', 2000)
        table.string('transactionNumber', 2000)
        table.boolean('is_show').defaultTo('true')
        table.integer('company_id').notNullable().references('company.id')
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('message')
}

