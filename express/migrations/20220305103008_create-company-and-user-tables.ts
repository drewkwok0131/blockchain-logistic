import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable('company')) return
    await knex.schema.createTable('company', table =>{
        table.increments('id')
        table.string('username', 64).notNullable().unique()
        table.string('password_hash', 60)
        table.string('email', 64).unique()
        table.string('public_key', 1800)
        table.string('private_key', 1800)
        table.timestamps(false,true)
    })

    if(await knex.schema.hasTable('staff')) return
    await knex.schema.createTable('staff', table =>{
        table.increments('id')
        table.string('username', 64).notNullable().unique()
        table.string('password_hash', 60)
        table.string('email', 64).unique()
        table.integer('company_id').notNullable().references('company.id')
        table.boolean('is_approved').defaultTo('false')
        table.timestamps(false,true)
    })

    if(await knex.schema.hasTable('partner_company')) return
    await knex.schema.createTable('partner_company', table =>{
        table.increments('id')
        table.string('name', 64).notNullable
        table.string('public_key', 1800)
        table.string('ip', 256)
    })

    if(await knex.schema.hasTable('channel_key')) return
    await knex.schema.createTable('channel_key', table =>{
        table.increments('id')
        table.integer('company_id').notNullable().references('company.id')
        table.string('shared_key', 256).notNullable()
        table.integer('partner_company_id').references('partner_company.id')
    })

    if(await knex.schema.hasTable('transaction')) return
    await knex.schema.createTable('transaction', table =>{
        table.increments('id')
        table.integer('company_id').notNullable().references('company.id')
        table.integer('staff_id').references('staff.id')
        table.integer('partner_company_id').references('partner_company.id')
        table.string('item_number', 64)
        table.string('item_name', 64)
        table.string('item_amount', 64)
        table.string('item_temperature', 64)
        table.string('test_report', 256)
        table.string('follow_up', 256)
        table.boolean('is_done')
        table.timestamps(false,true)
    })
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('transaction')
    await knex.schema.dropTableIfExists('channel_key')
    await knex.schema.dropTableIfExists('partner_company')
    await knex.schema.dropTableIfExists('staff')
    await knex.schema.dropTableIfExists('company')

}
