import express from "express";
import { wrapControllerMethod } from "./helpers";
import { HttpError } from "./httpError";
import { AccountInfoDTO, changePasswordDTO, LoginUserDTO, RegisterUsersDTO } from "./model";
import { UserService } from "./userService";

export class UserController {
    router = express.Router()

    constructor(private userService: UserService) {
        this.router.post('/login', wrapControllerMethod(this.loginAcc)),
        this.router.post('/register', wrapControllerMethod(this.registerAcc))
        this.router.post('/accountInfo', wrapControllerMethod(this.accountInfo))
        this.router.post('/changePW', wrapControllerMethod(this.changePW))
    }

    loginAcc = async (req: express.Request) => {
        let { username, password, role } = req.body
        if (!username) throw new HttpError('missing username in request body', 400)
        if (!password) throw new HttpError('missing password in request body', 400)
        if (!role) throw new HttpError('missing role in request body', 400)

        let user: LoginUserDTO = { username, password, role }
        let token = await this.userService.loginAcc(user)
        return { token }
    }

    registerAcc = async (req: express.Request) => {
        let { username, password, role, email, company_belong, serverSelected } = req.body
        if (!username) throw new HttpError('missing username in request body', 400)
        if (!password) throw new HttpError('missing password in request body', 400)
        if (!role) throw new HttpError('missing role in request body', 400)
        if (!email) throw new HttpError('missing email in request body', 400)
        if (role == 'staff' && !company_belong) throw new HttpError('missing company_belong in request body', 400)

        let user: RegisterUsersDTO = { username, password, role, email, company_belong, serverSelected} 
        let token = await this.userService.registerAcc(user)
        return { token }
    }

    accountInfo = async (req: express.Request) => {
        let {username} = req.body
        
        if (!username || username === undefined) throw new HttpError('missing username in request body', 400)
        let user: AccountInfoDTO = { username}
        
        let accountInfo = await this.userService.accountInfo(user)
        return accountInfo
    }

    changePW = async (req: express.Request) => {
        let {username,company, oldPassword, newPassword } = req.body
        let user: changePasswordDTO = {username,company, oldPassword, newPassword }
        console.log({username,company, oldPassword, newPassword });

        let changePWResult = await this.userService.changePW(user)
        return changePWResult


    }
}

