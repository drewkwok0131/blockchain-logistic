import { config } from "dotenv"

config();

export let env = {
    DB_NAME:process.env.DB_NAME,
    DB_USERNAME:process.env.DB_USERNAME,
    DB_PASSWORD:process.env.DB_PASSWORD,
    SESSION_SECRET : process.env.SESSION_SECRET,
    PORT: +process.env.PORT! || 8100,
    NODE_ENV:process.env.NODE_ENV || "development",
}
