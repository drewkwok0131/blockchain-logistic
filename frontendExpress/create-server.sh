npm init --yes

npm install \
  express \
	listening-on \
	express-session \
	dotenv \
  jest \
  knex \
  cors \
  pg

npm install -D \
  ts-node \
	typescript \
	ts-node-dev \
  ts-jest \
	@types/node \
	@types/express \
	@types/express-session \
  @types/jest \
  @types/pg \
  @types/cors

npm set-script start "ts-node-dev server.ts"

npx knex init -x ts

mkdir -p public

echo 'SESSION_SECRET=
PORT=
NODE_ENV=
DB_NAME=
DB_USERNAME=
DB_PASSWORD=
' > .env

echo 'import { config } from "dotenv"

config();

export let env = {
    DB_NAME:process.env.DB_NAME,
    DB_USERNAME:process.env.DB_USERNAME,
    DB_PASSWORD:process.env.DB_PASSWORD,
    SESSION_SECRET : process.env.SESSION_SECRET,
    PORT: +process.env.PORT! || 8100,
    NODE_ENV:process.env.NODE_ENV || "development",
}' > env.ts

echo '404 page not found' > public/404.html

echo 'node_modules
package-lock.json
pnpm-lock.yaml
.env
' > .gitignore

echo '404 page not found' > public/404.html

echo '{
  "compilerOptions": {
    "strict": true,
    "module": "commonjs",
    "target": "es5",
    "lib": ["es6", "dom"],
    "sourceMap": true,
    "allowJs": false,
    "jsx": "react",
    "esModuleInterop": true,
    "moduleResolution": "node",
    "noImplicitReturns": true,
    "noImplicitThis": true,
    "noImplicitAny": true,
    "strictNullChecks": true,
    "suppressImplicitAnyIndexErrors": true,
    "noUnusedLocals": true
  },
  "exclude": ["node_modules", "build", "scripts", "index.js"]
}
' > tsconfig.json

echo "import express from 'express'
import path from 'path'
import session from 'express-session'
import { print } from 'listening-on'
import {env} from './env'

let app = express()

if (!env.SESSION_SECRET) {
	throw new Error('missing SESSION_SECRET in env')
}

app.use(session({
	secret: env.SESSION_SECRET,
	resave: true,
	saveUninitialized: true,
}))

app.use(express.static('public'))

app.use((req, res)=>{
	res.sendFile(path.resolve(path.join('public', '404.html')))
})

let port = env.PORT

app.listen(port, () => {
  print(port)
})
" > server.ts

echo '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    hello world
</body>
</html>' > public/index.html

echo 'import Knex, { Knex as KnexInstance } from "knex"
import { env } from "./env"

let configs = require("./knexfile")
let mode = env.NODE_ENV
let config = configs[mode]

export let knex: KnexInstance = Knex(config)
' > db.ts