import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { useRef } from 'react';
import { Account } from '../components/Account';
import styles from './AccountPage.module.css';

const AccountPage: React.FC = () => {
  const routerRef = useRef<HTMLIonRouterOutletElement | null>(null);

  return (
    <IonPage>
      <IonHeader >
        <IonToolbar className={styles.headerDiv}>
          <IonTitle className={styles.title}>個人帳戶</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen className="ion-padding">
        <Account router={routerRef.current}></Account>
      </IonContent>
    </IonPage>
  );
};

export default AccountPage;
