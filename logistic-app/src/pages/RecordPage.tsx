import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { Record } from '../components/Record';
import styles from './RecordPage.module.css'

const RecordPage: React.FC = () => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar className={styles.headerDiv}>
                    <IonTitle>收貨記錄表</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent className="ion-padding">
                <Record></Record>
            </IonContent>
        </IonPage>
    );
};

export default RecordPage;