import { IonContent, IonPage } from '@ionic/react';
import { Login } from '../components/Login';
import './LoginPage.module.css';

const LoginPage: React.FC = () => {
  return (
    <IonPage>

      <IonContent fullscreen>
        <Login />

      </IonContent>
    </IonPage>
  );
};

export default LoginPage;
