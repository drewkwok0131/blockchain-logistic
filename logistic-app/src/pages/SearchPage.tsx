import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { Search } from '../components/Search';

const SearchPage: React.FC = () => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar style={{ '--background':"#FCF0E7"}}>
                    <IonTitle>搜尋</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent className="ion-padding">
                <Search></Search>
            </IonContent>
        </IonPage>
    );
};

export default SearchPage;