import { IonButton, IonContent, IonPage, IonText } from "@ionic/react";
import { routes } from "../routes";
import styles from './WelcomePage.module.css';
import logo from '../resources/blockchain.png'

const WelcomePage: React.FC = () => {
    return (
        <IonPage>
            <IonContent >

                <div className={styles.container}>
                    
                        <img style={{
                            width: '200px',
                            height: '200px',
                            margin: '80px 0 0 0'
                        }} src={logo} alt="" />
                    
                    <div className={styles.title}>
                        <IonText ><div>BlockChain</div><div>Logistic</div> <div>System</div></IonText>
                    </div>
                    <div className={styles.infoButtonDiv}>
                        <div className={styles.infoButton}>
                            <IonButton className={styles.buttonGroup1} routerLink={routes.register}>註冊用戶</IonButton>
                        </div>
                        <div className={styles.infoButton}>
                            <IonButton className={styles.buttonGroup1} routerLink={routes.login}>登入</IonButton>
                        </div>
                    </div>
                </div>

            </IonContent>
        </IonPage>
    );
};

export default WelcomePage;