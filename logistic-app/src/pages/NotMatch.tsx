import { IonButton, IonContent, IonHeader, IonPage, IonText, IonTitle, IonToolbar } from '@ionic/react';
import { useRouteMatch } from 'react-router';
import { routes } from '../routes';


const NotMatchPage: React.FC = () => {
    const route = useRouteMatch()
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>頁面並不存在</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <div>

                </div>
                <IonButton expand='block' routerLink={routes.login}>返回登入頁面</IonButton>
                <IonText>
                    <p>
                    {JSON.stringify(route.path)}  
                    </p>
                    <p>如需協助，請聯絡管理員</p>
          
                    </IonText>
            </IonContent>
        </IonPage>
    );
};

export default NotMatchPage;
