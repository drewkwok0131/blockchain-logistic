import { IonContent, IonPage } from '@ionic/react';
import { Register } from '../components/Register';
import './RegisterPage.module.css';

const RegisterPage: React.FC = () => {
  return (
    <IonPage>
      <IonContent fullscreen>
     <Register />
      </IonContent>
    </IonPage>
  );
};

export default RegisterPage;
