import { env } from "./env";

async function handleResponse(resP: Promise<Response>) {
    try {
        let res = await resP
        let json = await res.json()
        return json
    } catch (error) {
        return { error: String(error) }
    }
}

function fetchAPI(serverName:string){
    if (serverName === 'logistics1'){
        return env.API_ORIGIN1
    }
    if (serverName === 'logistics2'){
        return env.API_ORIGIN2
    }
    if (serverName === 'logistics3'){
        return env.API_ORIGIN3
    }
    if (serverName === 'logistics4'){
        return env.API_ORIGIN4
    }
    if (serverName === 'logisticsDev'){
        return env.API_ORIGIN_DEV
    }
    if (serverName === 'local'){
        return env.API_ORIGIN
    }
}

export function post(url: string, serverName: string, body?: any) {
    let resP = fetch(fetchAPI(serverName) + url, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(body)
    })

    return handleResponse(resP)
}

export async function postWithToken(
    token: string | null | undefined,
    url: string,
    serverName:string,
    body: any) {
    if (!token) {
        return ({ error: 'This API is not available to guess' }
        )
    }
    let resP = fetch(fetchAPI(serverName) + url, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer" + token
        },
        body: JSON.stringify(body)
    })
    return handleResponse(resP)
}