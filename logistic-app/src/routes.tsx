import { IonRouterOutlet, IonTabs } from "@ionic/react"
import { IonReactRouter } from "@ionic/react-router"
import { useSelector } from "react-redux"
import { Redirect, Route } from "react-router"
import AccountPage from "./pages/AccountPage"
import DefaultPage from "./pages/DefaultPage"
import LoginPage from "./pages/LoginPage"
import NotMatchPage from "./pages/NotMatch"
import RecordPage from "./pages/RecordPage"
import RegisterPage from "./pages/RegisterPage"
import SearchPage from "./pages/SearchPage"
import WelcomePage from "./pages/WelcomePage"
import { RootState } from "./redux/state"
import { IonReactHashRouter } from '@ionic/react-router'

import TabBar from "./TabBar"

export let routes = {
    tab: {
        account: "/tab/account",
        record: "/tab/record",
        search: "/tab/search",
        relationship: "/tab/relationship"
    },
    login: "/login",
    register: "/register",
    welcome: "/welcome"
}

//TODO: change it into companyRoute, staffRoute
// const GuestRoute = (props:{
//     exact?:boolean,
//     path:string,
//     component: React.FC
// }) =>{
//     const role = useSelector((state: RootState)=>state.auth.user?.role)
// // company ,staff
//     const isGuest = useSelector((state: RootState)=>!state.auth.user)
//     const Component = props.component
//     return <Route exact={props.exact} path={props.path}>
//         {isGuest? <Component/>: role==='company'? <Redirect to={routes.tab.search}></Redirect>:<Redirect to={routes.tab.record}></Redirect>}
//         </Route>
// }



const GuestRoute = (props: {
    exact?: boolean,
    path: string,
    component: React.FC
}) => {
    const isGuest = useSelector((state: RootState) => !state.auth.user)
    const Component = props.component
    return <Route exact={props.exact} path={props.path}>
        {isGuest ? <Component /> : <Redirect to={routes.tab.record}></Redirect>}
    </Route>
}


export const Routes = () => {
    return (
        <>
            <IonReactRouter>
                <IonReactHashRouter>
                    <IonRouterOutlet>
                        <Route exact path='/'>
                            <DefaultPage />
                            {/* <Redirect to={routes.welcome} /> */}
                        </Route>
                        <GuestRoute exact path={routes.login} component={LoginPage} />
                        <GuestRoute exact path={routes.register} component={RegisterPage} />
                        <GuestRoute exact path={routes.welcome} component={WelcomePage} />
                    </IonRouterOutlet>

                    <Route path='/tab'>
                        <IonTabs>
                            <IonRouterOutlet>
                                <Route exact path={routes.tab.record} component={RecordPage} />
                                <Route exact path={routes.tab.search} component={SearchPage} />
                                <Route path={routes.tab.account} component={AccountPage} />
                                <Route>
                                    <NotMatchPage />
                                </Route>
                            </IonRouterOutlet>
                            {TabBar()}
                        </IonTabs>
                    </Route>
                </IonReactHashRouter>
            </IonReactRouter>

        </>
    )
}