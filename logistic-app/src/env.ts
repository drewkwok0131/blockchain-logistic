// if (!process.env.REACT_APP_API_ORIGIN) {
//     throw new Error('missing REACT_APP_API_ORIGIN')
// }

export let env = {
    API_ORIGIN: process.env.REACT_APP_API_ORIGIN,
    API_ORIGIN1: process.env.REACT_APP_LOGISTICS1,
    API_ORIGIN2: process.env.REACT_APP_LOGISTICS2,
    API_ORIGIN3: process.env.REACT_APP_LOGISTICS3,
    API_ORIGIN4: process.env.REACT_APP_LOGISTICS4,    
    API_ORIGIN_DEV: process.env.REACT_APP_LOGISTICS_DEV 
}