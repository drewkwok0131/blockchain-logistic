import { IonTabBar, IonTabButton, IonIcon, IonLabel } from "@ionic/react"
import { documentTextOutline, searchOutline, peopleOutline } from "ionicons/icons"
import { routes } from "./routes"

const TabBar = () => {
    return (

            <IonTabBar slot="bottom" className="tabBar">
                <IonTabButton tab="recordTab" href={routes.tab.record}>
                    <IonIcon icon={documentTextOutline} />
                    <IonLabel>記錄表</IonLabel>
                </IonTabButton>
                <IonTabButton tab="searchTab" href={routes.tab.search}>
                    <IonIcon icon={searchOutline} />
                    <IonLabel>搜尋</IonLabel>
                </IonTabButton>
                <IonTabButton tab="accountTab" href={routes.tab.account}>
                    <IonIcon icon={peopleOutline} />
                    <IonLabel>帳戶</IonLabel>
                </IonTabButton>
            </IonTabBar>

    )
}

export default TabBar