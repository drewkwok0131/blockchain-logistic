
import { Dispatch } from "redux";
import { AccountInfoAction } from "./accountInfo/action";
import { AuthAction } from "./auth/action";

export type AppAction = AuthAction | AccountInfoAction

export type AppDispatch = Dispatch<AppAction>


