import { rootReducer } from "./reducer";
import { createStore } from 'redux' 
import { rootEnhancer } from "./enhancer";

export let store = createStore(rootReducer, rootEnhancer)