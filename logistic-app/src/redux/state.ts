import { AccountState } from "./accountInfo/state";
import { AuthState } from "./auth/state";

export type RootState = {
    auth: AuthState
    accountInfo: AccountState
}