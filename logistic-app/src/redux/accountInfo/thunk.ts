import { post } from "../../api";
import { AppDispatch } from "../action";
import { RootState } from "../state";
import { accountInfoAction } from "./action";

export function accountInfoThunk(user:{username:string, serverName:string}){
    return async (dispatch: AppDispatch, getState:()=>RootState) => {
        let json = await post('/accountInfo',user.serverName, user)
        
        localStorage.setItem('accountInfo',json)
        dispatch(accountInfoAction({email:json.email,companyName:json.companyName}))
    }
}