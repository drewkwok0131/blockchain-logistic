export type AccountState = {
    email: string
    companyName: string
}