import { AccountInfoAction } from "./action";
import { AccountState } from "./state";

let initialState = {
    email: '',
    companyName: ''
}

export function accountInfoReducer(
    state: AccountState = initialState,
    action : AccountInfoAction
):AccountState {
    switch (action.type) {
        case '@@accountInfo/info':
            return {
                email: action.info.email,
                companyName: action.info.companyName
            }
        default:
            return state
    }
}