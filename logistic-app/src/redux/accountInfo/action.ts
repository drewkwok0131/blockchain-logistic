export function accountInfoAction(info: {email: string, companyName: string, }){
    return {type: '@@accountInfo/info' as const, info}
}

export type AccountInfoAction = 
| ReturnType<typeof accountInfoAction>