import { authReducer } from "./auth/reducer";
import { combineReducers } from 'redux'
import { RootState } from "./state";
import { accountInfoReducer } from "./accountInfo/reducer";

export const rootReducer = combineReducers<RootState, any>({
    auth: authReducer,
    accountInfo: accountInfoReducer
})