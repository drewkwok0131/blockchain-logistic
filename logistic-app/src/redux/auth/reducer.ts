import { AuthAction } from "./action";
import { AuthState, AuthUser, JWTPayload } from "./state";
import jwtDecode from 'jwt-decode';

function loadUserFromToken(token: string | null): AuthUser | null {
    if (!token) return null
    try {
        let payload: JWTPayload = jwtDecode(token)
        return { username: payload.username, role: payload.role , token }
    } catch (error) {
        console.error('failed to decode JST: ', error)
        return null
    }
}

function initialState(): AuthState {
    let token = localStorage.getItem('token')
    return {
        user: loadUserFromToken(token),
        registerResult: { type: 'idle' },
        loginResult: { type: 'idle' },
        serverSelected: null
    }
}

export function authReducer(
    state: AuthState = initialState(),
    action: AuthAction
): AuthState {
    switch (action.type) {
        case '@@Auth/logout':
            return { ...state, user: null, loginResult: { type: 'idle' } }
        case '@@Auth/setRegisterResult':
            return {
                ...state,
                registerResult: action.result,
                user: loadUserFromToken(
                    action.result.type === 'success' ?
                        action.result.token : null
                )
            }
        case '@@Auth/setLoginResult':
            return {
                ...state,
                loginResult: action.result,
                user: loadUserFromToken(
                    action.result.type === 'success' ?
                        action.result.token : null
                )
            }
        case '@@Auth/setSelectedServer':
            
            return {
                ...state,
                serverSelected:action.serverName
            }
        default:
            return state
    }
}

    //     case '@@Auth/register':
    //         if (action.user.username in state.userPasswordDict) {
    //             return {
    //                 ...state, registerResult: {
    //                     type: 'fail', message: `「${action.user.username}」已有其他用戶使用`
    //                 }
    //             }
    //         }
    //         localStorage.setItem('username', action.user.username)
    //         return {
    //             ...state, user: { username: action.user.username },
    //             userPasswordDict: {
    //                 ...state.userPasswordDict,
    //                 [action.user.username]: action.user.password
    //             },
    //             registerResult: {
    //                 type: 'success', message: '你已成功註冊'
    //             }
    //         }
    //     case "@@Auth/login": {
    //         if (!(action.user.username in state.userPasswordDict)) {
    //             return {
    //                 ...state, loginResult: {
    //                     type: 'fail', message: '此用戶未有登記，請重新輸入或註冊'
    //                 }
    //             }
    //         }
    //         if (state.userPasswordDict[action.user.username] !== action.user.password) {
    //             return {
    //                 ...state, loginResult: {
    //                     type: 'fail', message: '密碼錯誤，請重新輸入'
    //                 }
    //             }
    //         }
    //         localStorage.setItem('username', action.user.username)
    //         return {
    //             ...state,
    //             user: { username: action.user.username },
    //             loginResult: {
    //                 type: 'success', message: `${action.user.username}成功登入`
    //             }
    //         }
    //     }

    //     default: return state

    // }
