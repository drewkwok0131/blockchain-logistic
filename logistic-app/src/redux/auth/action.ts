import { APIResultType } from "./state";

export function logoutAction(){
    return {type: '@@Auth/logout' as const}
}

export function setLoginResultAction(result: APIResultType){
    return {type: '@@Auth/setLoginResult' as const, result }
}

export function setRegisterResultAction(result: APIResultType){
    return {type: '@@Auth/setRegisterResult' as const, result }
}

export function setSelectedServerAction(serverName: string){
    return {type: '@@Auth/setSelectedServer' as const, serverName }
}

export type AuthAction = 
| ReturnType<typeof setRegisterResultAction>
| ReturnType<typeof setLoginResultAction>
| ReturnType<typeof logoutAction>
| ReturnType<typeof setSelectedServerAction>