

export type AuthState = {
    user: null | AuthUser
    registerResult: APIResultType
    loginResult: APIResultType
    serverSelected: string | null
}

export type APIResultType =
    | { type: 'idle' }
    | { type: 'success'; token:string }
    | { type: 'fail'; message: string }


export type AuthUser = {
    username: string
    token: string | null
    role: string
}

export type JWTPayload = {
    id: number
    username: string
    role: string
}