
import { post } from "../../api";
import { AppDispatch } from "../action";
import { RootState } from "../state";
import { logoutAction, setLoginResultAction, setRegisterResultAction } from "./action";

export function logoutThunk(){
    return async (dispatch: AppDispatch, getState: () => RootState) =>{
        localStorage.removeItem('token')
        dispatch(logoutAction())
    }
}

export function loginThunk(user: {
    username: string,
    password: string,
    serverSelected: string,
    role: string
}) {
    return async (dispatch: AppDispatch, getState: () => RootState) => {
        let json = await post('/login', user.serverSelected, user)
        if (json.error) {
            dispatch(setLoginResultAction({
                type: "fail",
                message: json.error
            }))
        } else {
            localStorage.setItem('token', json.token)
            dispatch(setLoginResultAction({
                type: 'success',
                token: json.token
            }))
        }
    }
}

export function registerThunk(user: {
    username: string,
    password: string,
    role: string,
    email: string,
    serverSelected: string,
    company_belong?: string
}) {
    return async (dispatch: AppDispatch, getState: () => RootState) => {
        let json = await post('/register', user.serverSelected, user)
        if (json.error) {
            dispatch(setRegisterResultAction({
                type: "fail",
                message: json.error
            }))
        } else {
            localStorage.setItem('token', json.token)
            dispatch(setRegisterResultAction({
                type: 'success',
                token: json.token
            }))
        }
    }
}