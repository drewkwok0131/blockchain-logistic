import { IonList, IonItem, IonLabel, IonInput, IonSelect, IonSelectOption, IonText, IonButton, useIonAlert } from "@ionic/react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { registerThunk } from "../redux/auth/thunk";
import { RootState } from "../redux/state";
import { routes } from "../routes";
import { APIResult } from "./APIresultMessage";
import styles from "../pages/RegisterPage.module.css";
import { setSelectedServerAction } from "../redux/auth/action";

type FormState = {
    username: string
    password: string
    confirm_password: string
    email: string
    role: string
    company_belong?: string
    serverSelected: string
}

export function Register() {

    const dispatch = useDispatch()
    const [presentAlert] = useIonAlert()
    const result = useSelector((state: RootState) => state.auth.registerResult)

    const [formState, setFormState] = useState<FormState>({
        username: '',
        password: '',
        confirm_password: '',
        email: '',
        role: '',
        company_belong: '',
        serverSelected: '',
    })


    const messages = {
        username: formState.username.length < 3 ? '用戶名稱須不少於3個英文字母' : '',
        password: formState.password.length < 8 ? '密碼須最少8個字元' : '',
        confirm_password: formState.confirm_password !== formState.password ? '密碼錯誤，請重新輸入' : '',
        email: !formState.email || !formState.email.includes('@') ? '請填寫有效的電郵地址' : '',
        role: !formState.role ? '請選擇身分' : '',
        company_belong: !formState.company_belong && formState.role === 'staff' ? '請填寫公司名稱' : '',
        serverSelected: !formState.serverSelected ? '請選擇伺服器' : ''
    }

    const submit = () => {
        console.log('formState: ', formState);
        const invalidMessage = Object.values(messages).find(message => message.length > 0)
        if (invalidMessage) {
            presentAlert(invalidMessage, [{ text: '確定', role: "cancel" }])
            return
        }
        dispatch(setSelectedServerAction(formState.serverSelected))
        dispatch(registerThunk(formState))
    }

    const renderField = (props: {
        field: keyof FormState
        inputType: 'text' | 'password'
        showName: '用戶名稱' | '密碼' | '確認密碼' | '電郵地址'
    }) => {
        const { field, inputType, showName } = props
        const value = formState[field]
        const validationMessage = messages[field]

        return (
            <>
                <IonItem>
                    <IonLabel
                        position="floating"
                        color={
                            !value ? (hintField === field ? 'primary' : undefined) : validationMessage ? 'danger' : undefined}>
                        {showName}
                    </IonLabel>
                    <IonInput
                        type={inputType}
                        value={value}
                        onIonChange={e => setFormState({ ...formState, [field]: e.detail.value || '' })}>
                    </IonInput>
                </IonItem>
                <IonText color={
                    !value ? 'light' : 'danger'
                }>{validationMessage}</IonText>
            </>
        )
    }

    function getHintField() {
        for (let [key, value] of Object.entries(formState)) {
            const field = key as keyof FormState
            const validationMessage = messages[field]
            if (!value || validationMessage) {
                return field
            }
        }
    }
    let hintField = getHintField()

    return (
        <div className='container'>
            <div className={styles.header}>
                註冊
            </div>
            <div>
                <IonList>
                    {renderField({ field: 'username', inputType: "text", showName: '用戶名稱' })}
                    {renderField({ field: 'password', inputType: "password", showName: '密碼' })}
                    {renderField({ field: 'confirm_password', inputType: "password", showName: '確認密碼' })}
                    {renderField({ field: 'email', inputType: "text", showName: '電郵地址' })}
                </IonList>
                <IonList>
                    <IonItem>
                        <IonLabel>身份</IonLabel>
                        <IonSelect
                            interface="alert"
                            value={formState.role} placeholder="請選擇其中一個"
                            onIonChange={e => setFormState({ ...formState, role: e.detail.value || '' })}>
                            <IonSelectOption value="company">公司</IonSelectOption>
                            <IonSelectOption value="staff">員工</IonSelectOption>
                        </IonSelect>
                    </IonItem>
                </IonList>
                <IonList>
                    <IonItem hidden={formState.role !== 'staff' ? true : undefined} >
                        <IonLabel
                            position="floating">所屬公司</IonLabel>
                        <IonInput value={formState.company_belong} onIonChange={e => setFormState({ ...formState, company_belong: e.detail.value || '' })} ></IonInput>
                    </IonItem>
                </IonList>
                <IonList>
                    <IonItem>
                        <IonLabel>連接伺服器</IonLabel>
                        <IonSelect
                            interface="alert"
                            value={formState.serverSelected} placeholder="請選擇其中一個"
                            onIonChange={e => setFormState({ ...formState, serverSelected: e.detail.value || '' })}>
                            <IonSelectOption value="logistics1">Logistics 1</IonSelectOption>
                            <IonSelectOption value="logistics2">Logistics 2</IonSelectOption>
                            <IonSelectOption value="logistics3">Logistics 3</IonSelectOption>
                            <IonSelectOption value="logistics4">Logistics 4</IonSelectOption>
                        </IonSelect>
                    </IonItem>
                </IonList>
                <div className={styles.infoButtonDiv}>
                    <div className={styles.infoButton}>
                        <IonButton className={styles.buttonGroup1} onClick={submit}>確定註冊</IonButton>
                    </div>
                    <div className={styles.infoButton}>
                        <IonButton className={styles.buttonGroup2} routerLink={routes.login} >已有用戶？登入</IonButton>
                    </div>
                </div>


                <APIResult result={result} />
                {/* {console.log('result: ', result)} */}
            </div>
        </div>
    )
}

