import { IonAlert, IonButton, IonInput, IonItem, IonLabel, IonList, useIonAlert, IonSpinner } from "@ionic/react"
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { post } from "../api"
import { accountInfoThunk } from "../redux/accountInfo/thunk"
import { RootState } from "../redux/state"
import styles from './Record.module.css'
import loading from '../resources/loading.gif'

type RecordFormState = {
    personInCharge: string
    companyInCharge: string
    receiver: string
    productionNo: string
    productionName: string
    productionQuantity: string
    testReport: string
    productionTemp: string
    remark: string
    is_done: boolean
}

export function Record() {
    const userInfo = useSelector((state: RootState) => {
        return {
            username: state.auth.user?.username,
            company: state.accountInfo.companyName,
            serverSelected: state.auth.serverSelected
        }
    })
    const dispatch = useDispatch()
    const [recordFormState, setRecordFormState] = useState<RecordFormState>({
        personInCharge: '',
        companyInCharge: '',
        receiver: '',
        productionNo: '',
        productionName: '',
        productionQuantity: '',
        testReport: '',
        productionTemp: '',
        remark: '',
        is_done: false,
    })

    const [presentAlert] = useIonAlert()
    const [showAlert, setShowAlert] = useState(false)
    const [errorMessage, setErrorMessage] = useState('')
    const [showLoading , setShowLoading] = useState(false)
    useEffect(() => {
        dispatch(accountInfoThunk({ username: userInfo!.username!, serverName: userInfo.serverSelected! }))
        setRecordFormState({
            ...recordFormState,
            personInCharge: userInfo.username!,
            companyInCharge: userInfo.company
        })
    }, [])

    const renderField = (props: {
        field: keyof RecordFormState
        inputType: 'text'
        showName: '收貨公司' | '貨品編號' | '貨品名稱' | '貨品數量' | '檢驗報告' | '貨品溫度' | '跟進行動(選填)'
    }) => {
        const { field, inputType, showName } = props
        const value = recordFormState[field]

        return (
            <>
                <IonList>
                    <IonItem>
                        <IonLabel className={styles.textContent} position='floating'>{showName}</IonLabel>
                        <IonInput
                            type={inputType}
                            value={value as string}
                            onIonChange={e => {
                                setRecordFormState({ ...recordFormState, [field]: e.detail.value || '' }); console.log('record form: ', recordFormState);
                            }}
                        ></IonInput>
                    </IonItem>
                </IonList>
            </>
        )
    }

    const submitForm = async () => {
        setShowLoading(true)
        let requiredData = ['receiver', 'productionNo', 'productionName', 'productionQuantity', 'testReport', 'productionTemp']
        let errorList = []
        let errorMessage = ''

        for (let data of requiredData) {
            if (recordFormState[data as keyof RecordFormState] === '') {
                errorList.push(data)
            }
        }

        for (let error of errorList) {
            errorMessage = error + ',' + errorMessage
        }

        setErrorMessage(errorMessage)

        if (errorList.length === 0) {
            //console.log(recordFormState);
            let result
            if (userInfo.serverSelected) {
                result = await post('/record', userInfo.serverSelected, { ...recordFormState, serverSelected: userInfo.serverSelected })
                setShowLoading(false)
            }
            console.log('result: ', result.recordResult.message);
            if (result.recordResult.message === 'record received') {
                setRecordFormState({
                    ...recordFormState,
                    receiver: '',
                    productionNo: '',
                    productionName: '',
                    productionQuantity: '',
                    testReport: '',
                    productionTemp: '',
                    remark: '',
                    is_done: false,
                })
                //console.log(result.recordResult.id);

                let id = result.recordResult.transactionID.id
                presentAlert(`紀錄已成功送出！私人數據庫表格ID及區塊鏈表格ID：${id} ; ${'transaction' + id + userInfo.serverSelected}`, [{ text: '確定', role: "cancel" }])

            } else if (result.recordResult.message === 'cannot find this company') {
                presentAlert('你輸入的收貨公司並不存在，請檢查並重新輸入', [{ text: '確定', role: "cancel" }])
            } else {
                presentAlert('輸入資料有誤，請檢查並重新輸入', [{ text: '確定', role: "cancel" }])
            }
        } else {
            setShowAlert(true)
        }
    }


    return (
        <>
            <div className={styles.containerDiv}>
                <IonList>
                    <IonItem>
                        <IonLabel position='floating'>負責人員</IonLabel>
                        <IonInput
                            readonly
                            value={userInfo.username}
                            onIonChange={e => { setRecordFormState({ ...recordFormState, personInCharge: e.detail.value || '' }) }}
                        ></IonInput>
                    </IonItem>
                </IonList>
                <IonList>
                    <IonItem>
                        <IonLabel position='floating'>負責公司</IonLabel>
                        <IonInput
                            readonly
                            value={userInfo.company}
                            onIonChange={e => { setRecordFormState({ ...recordFormState, companyInCharge: e.detail.value || '' }) }}
                        ></IonInput>
                    </IonItem>
                </IonList>

                {renderField({ field: 'receiver', inputType: 'text', showName: '收貨公司' })}
                {renderField({ field: 'productionNo', inputType: 'text', showName: '貨品編號' })}
                {renderField({ field: 'productionName', inputType: 'text', showName: '貨品名稱' })}
                {renderField({ field: 'productionQuantity', inputType: 'text', showName: '貨品數量' })}
                {renderField({ field: 'testReport', inputType: 'text', showName: '檢驗報告' })}
                {renderField({ field: 'productionTemp', inputType: 'text', showName: '貨品溫度' })}
                {renderField({ field: 'remark', inputType: 'text', showName: '跟進行動(選填)' })}

                <IonButton className={styles.submitButton} onClick={submitForm}>提交</IonButton>

                <IonAlert
                    isOpen={showAlert}
                    onDidDismiss={() => setShowAlert(false)}
                    cssClass='my-custom-class'
                    header={'錯誤'}
                    subHeader={'以下填寫欄不能空白'}
                    message={errorMessage}
                    buttons={['OK']}
                />
            </div>
            <div hidden={!showLoading} className={styles.loadingDiv}>
                <IonSpinner className={styles.loadingCircles} name="crescent"></IonSpinner>
            </div>
        </>
    )
}