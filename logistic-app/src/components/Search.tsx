import { IonAlert, IonButton, IonInput, IonItem, IonLabel, IonList, IonSelect, IonSelectOption, IonSpinner, useIonAlert } from "@ionic/react";
import { useState } from "react";
import { useSelector } from "react-redux";
import { post } from "../api";
import { RootState } from "../redux/state";
import styles from "./Search.module.css"

type SelectMethod = "transactionID" | "productionNo" | "personInCharge" | "self-history" | ''
type Database = 'psql' | 'blockchain' | ''
type SelectOptionState = {
    frontpage: boolean
    database: boolean
    method: boolean
    output: boolean
}
type UserInfo = {
    username: string | ''
    serverSelected: string
    role: string
}

type ResultList = {
    result: Array<Transaction>
}

type Transaction = {
    companyInCharge: string
    created_time: string
    id: Number
    is_done: boolean | string
    personInCharge: string
    productionName: string
    productionNo: string
    productionQuantity: string
    productionTemp: string
    receiver: string
    remark: string
    testReport: string
    updated_time: string
    username?: string
}

export function Search() {
    const [resultList, setResultList] = useState<ResultList>()
    const [method, setMethod] = useState('')
    const [presentAlert] = useIonAlert()
    const [transactionID, setTransactionID] = useState('')
    const [productionNo, setProductionNo] = useState('')
    const [personInCharge, setPersonInCharge] = useState('')
    const [database, setDatabase] = useState<Database>('')
    const [showAlert, setShowAlert] = useState(false)
    const [showLoading, setShowLoading] = useState(false)
    const [showSelectOption, setShowSelectOption] = useState<SelectOptionState>({
        frontpage: true,
        database: true,
        method: false,
        output: false,
    })

    async function saveToDB(result: Transaction) {
        console.log(result);
        let res = await post('/savetodb', userInfo.serverSelected, result)
        console.log(res);

        if (res.message === 'this form is created by yourself, no need to save again') {
            presentAlert('此貨單由你建立，毋須再次放入數據庫', [{ text: '確定', role: "cancel" }])
            return
        }
        if (res.message === 'saved successfully') {
            presentAlert('已經成功將資料放入數據庫', [{ text: '確定', role: "cancel" }])
            return
        }
        if (res.message === 'cannot decrypt message by using channel key') {
            presentAlert('你未能解密資料，此項交易應該與 貴公司無關，無儲存數據的需要', [{ text: '確定', role: "cancel" }])
            return
        }
        if (res.message === 'data is already in database') {
            presentAlert('資料已在數據庫中，毋須再次儲存', [{ text: '確定', role: "cancel" }])
            return
        }
        if (res.message === 'cannot find sharedKey') {
            presentAlert('你未能解密資料，此項交易應該與 貴公司無關，無儲存數據的需要', [{ text: '確定', role: "cancel" }])
            return
        } else {
            presentAlert('你未能解密資料，數據未有儲存', [{ text: '確定', role: "cancel" }])
            return
        }

    }

    function genResultList(result: Transaction) {
        console.log('genResultList: ', result);
        let username = userInfo.username
        result = { ...result, username }

        return (

            <>
                <div key={result.id.toString()} className={styles.resultContainer}>
                    <div><b>交易ID：</b>{result.id}</div>
                    <div><b>建立時間：</b>{result.created_time}</div>
                    <div><b>更新時間：</b>{result.updated_time}</div>
                    <div><b>負責公司：</b>{result.companyInCharge}　<b>負責人員：</b>{result.personInCharge}</div>
                    <div><b>收貨公司：</b>{database === 'blockchain' ? '加密資料' : result.receiver}</div>
                    <div><b>貨品編號：</b>{result.productionNo}　
                        <b>貨品名稱：</b>{database === 'blockchain' ? '加密資料' : result.productionName}　
                        <b>貨品數量：</b>{database === 'blockchain' ? '加密資料' : result.productionQuantity}　
                        <b>貨品溫度：</b>{result.productionTemp}　
                        <b>檢驗報告：</b>{result.testReport}　
                        <b>跟進行動：</b>{result.remark === '' ? '沒有' : result.remark}
                    </div>
                    <div><b>已確認　：</b>{result.is_done === false || result.is_done === 'false' ? "否" : '是'}</div>
                    {database === 'blockchain' ?
                        <IonButton className={styles.saveIonButton} onClick={() => saveToDB(result)} >儲存至數據庫</IonButton>
                        : null
                    }
                </div>
            </>

        )
    }

    const userInfo = useSelector((state: RootState): UserInfo => {
        return {
            username: state.auth.user?.username || '',
            serverSelected: state.auth.serverSelected || '',
            role: state.auth.user?.role || ''
        }
    })

    // generate input field
    function genField(method: SelectMethod) {
        if (method === 'transactionID') {
            return (
                <>
                    <IonLabel position='floating'>交易 ID</IonLabel>
                    <IonInput
                        value={transactionID}
                        onIonChange={e => { setTransactionID(e.detail.value || '') }}
                        style={{ 'borderBottom': '1px solid #686868', 'width': '100%' }}>
                    </IonInput>
                </>
            )
        } else if (method === 'productionNo') {
            return (
                <>
                    <IonLabel position='floating'>貨品編號</IonLabel>
                    <IonInput
                        value={productionNo}
                        onIonChange={e => { setProductionNo(e.detail.value || '') }}
                        style={{ 'borderBottom': '1px solid #686868', 'width': '100%' }}>
                    </IonInput>
                </>
            )
        } else if (method === 'personInCharge') {
            return (
                <>
                    <IonLabel position='floating'>負責人</IonLabel>
                    <IonInput
                        value={personInCharge}
                        onIonChange={e => { setPersonInCharge(e.detail.value || '') }}
                        style={{ 'borderBottom': '1px solid #686868', 'width': '100%' }}>
                    </IonInput>
                </>
            )
        } else if (method === 'self-history') {
            return (
                <>
                    <IonLabel position='floating'>個人紀錄</IonLabel>
                    <IonInput
                        readonly
                        value={userInfo.username ? userInfo.username : ''}
                        style={{ 'borderBottom': '1px solid #686868', 'width': '100%' }}>
                    </IonInput>
                </>
            )
        } else {
            return (
                <>
                    <IonLabel position='floating'></IonLabel>
                    <p>請先選擇搜尋方法</p>
                </>)
        }
    }

    // function that return search data for fetching to blockchain
    function getFormData(method: SelectMethod) {
        if (method === 'transactionID' && transactionID !== '') return { database: database, method: method, data: transactionID }
        if (method === 'productionNo' && productionNo !== '') return { database: database, method: method, data: productionNo }
        if (method === 'personInCharge' && personInCharge !== '') return { database: database, method: method, data: personInCharge }
        if (method === 'self-history') return { database: database, method: method, data: userInfo?.username }
        setShowAlert(true)
        return null
    }

    async function submitForm(method: SelectMethod) {
        try {
            setShowLoading(true)
            const formData = getFormData(method)
            if (formData === null) return
            if (!userInfo.serverSelected) {
                presentAlert('沒有選擇伺服器，請重新登入', [{ text: '確定', role: "cancel" }])
                return
            }
            if (formData.database === '') {
                presentAlert('請選擇數據庫或區塊鏈', [{ text: '確定', role: "cancel" }])
                return
            }
            let result = formData.database === 'blockchain' ?
                await post('/search/blockchain', userInfo.serverSelected, formData) :
                await post('/search/psql', userInfo.serverSelected, formData)

            setShowLoading(false)

            if (result.message === 'no transaction record') {
                presentAlert('沒有記錄', [{ text: '確定', role: "cancel" }])
                return
            }
            console.log("submitFormResult: ", result);

            setResultList(result)
            setShowSelectOption({ ...showSelectOption, method: false, output: true })

        } catch (e) {
            presentAlert('未能找到相關資料，請重新輸入', [{ text: '確定', role: "cancel" }])
            return
        }
    }



    return (
        <>
            {showSelectOption.frontpage === true ?
                <div className={styles.databaseButtonDiv} hidden={!showSelectOption.database}>
                    <IonList lines='none'>
                        <IonItem>
                            <IonButton className={styles.databaseIonButton} onClick={() => {
                                setShowSelectOption({ ...showSelectOption, database: false, method: true, frontpage: false })
                                setDatabase('blockchain')
                            }
                            }>使用區塊鏈搜尋</IonButton>
                        </IonItem>
                        <IonItem>
                            <IonButton className={styles.databaseIonButton} onClick={() => {
                                setShowSelectOption({ ...showSelectOption, database: false, method: true, frontpage: false })
                                setDatabase('psql')
                            }
                            }>使用數據庫搜尋</IonButton>
                        </IonItem>
                    </IonList>
                </div>
                : null}

            {showSelectOption.method === true ?
                (<>
                    <IonItem lines="none" className={styles.methodSelection}>
                        <IonLabel>搜尋方法</IonLabel>
                        <IonSelect
                            interface="alert"
                            value={method} placeholder="請選擇其中一個"
                            onIonChange={e => setMethod(e.detail.value)}>
                            <IonSelectOption value="transactionID">交易ID</IonSelectOption>
                            <IonSelectOption value="productionNo">貨品編號</IonSelectOption>
                            <IonSelectOption value="personInCharge">負責人</IonSelectOption>
                            <IonSelectOption value="self-history">個人紀錄</IonSelectOption>
                        </IonSelect>
                    </IonItem>

                    <IonItem lines="none" className={styles.inputDiv}>
                        <div className={styles.inputTitle}>輸入關鍵字</div>
                        <div className={styles.inputContent}>
                            {genField(method as SelectMethod)}
                        </div>
                    </IonItem>

                    <div className={styles.searchButtonDiv}>
                        <IonButton className={styles.searchIonButton}
                            onClick={() => userInfo.role === 'company' ? submitForm(method as SelectMethod) : presentAlert('抱歉！你未有此用戶權限', [{ text: '確定', role: "cancel" }])}
                        >搜尋</IonButton>
                    </div>
                    <div className={styles.searchButtonDiv}>
                        <IonButton className={styles.searchIonButton}
                            onClick={() => setShowSelectOption({ ...showSelectOption, database: true, method: false, frontpage: true })}
                        >返回</IonButton>
                    </div>
                </>) :
                null}

            {showSelectOption.output === true ?
                resultList ?
                    <>
                        <div className={styles.resultOrder}>
                            {resultList.result.map(obj => genResultList(obj))}
                        </div>
                        <div className={styles.searchButtonDiv}>
                            <IonButton className={styles.searchIonButton}
                                onClick={() => setShowSelectOption({ ...showSelectOption, database: true, frontpage: true, output: false })}
                            >返回</IonButton>
                        </div>
                    </>
                    : null
                : null
            }

            <div hidden={!showLoading} className={styles.loadingDiv}>
                <IonSpinner className={styles.loadingCircles} name="crescent"></IonSpinner>
            </div>


            {/* for user that do not input parameter */}
            <IonAlert
                isOpen={showAlert}
                onDidDismiss={() => setShowAlert(false)}
                cssClass='my-custom-class'
                header={'錯誤'}
                message={'請選擇及輸入關鍵字'}
                buttons={['OK']}
            />
        </>
    )
}
