import { IonButton, IonInput, IonItem, IonLabel, IonList, IonSelect, IonSelectOption, useIonAlert } from "@ionic/react";
import { useState } from "react"
import { useDispatch, useSelector } from "react-redux";
import { loginThunk } from "../redux/auth/thunk";
import { RootState } from "../redux/state";
import { routes } from "../routes";
import { APIResult } from "./APIresultMessage";
import styles from "../pages/LoginPage.module.css";
import { setSelectedServerAction } from "../redux/auth/action";




export function Login() {

    const result = useSelector((state: RootState) => state.auth.loginResult)
    const dispatch = useDispatch()
    const [presentAlert] = useIonAlert()

    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [role, setRole] = useState('')
    const [serverSelected, setServerSelected] = useState('')

    let user = {
        username: username,
        password: password,
        role: role,
        serverSelected: serverSelected
    }

    const messages = {
        username: user.username ? '' : '請輸入用戶名稱',
        password: user.password ? '' : '請輸入密碼',
        role: user.role? '' : '請選擇身分',
        serverSelected: user.serverSelected? '' : '請選擇伺服器'
    }

    const submit = () => {
        const invalidMessage = Object.values(messages).find(message => message.length > 0)
        if (invalidMessage) {
            presentAlert(invalidMessage, [{ text: '確定', role: "cancel" }])
            return
        }
        console.log(user.serverSelected)
        dispatch(setSelectedServerAction(user.serverSelected))
        dispatch(loginThunk(user))
    }

    return (
        <div className="container">
            <div className={styles.header}>登入</div>
            <div>
                <IonList>
                    <IonItem>
                        <IonLabel className={styles.textContent}>身份</IonLabel>
                        <IonSelect
                            interface="alert"
                            value={role} placeholder="請選擇其中一個"
                            onIonChange={e => setRole(e.detail.value || '')}>
                            <IonSelectOption value="company">公司</IonSelectOption>
                            <IonSelectOption value="staff">員工</IonSelectOption>
                        </IonSelect>
                    </IonItem>
                </IonList>
                <IonList>
                    <IonItem>
                        <IonLabel position="floating">名稱</IonLabel>
                        <IonInput type="text" value={username} onIonChange={e => setUsername(e.detail.value || '')}></IonInput>
                    </IonItem>
                </IonList>
                <IonList>
                    <IonItem>
                        <IonLabel position="floating">密碼</IonLabel>
                        <IonInput type="password" value={password} onIonChange={e => setPassword(e.detail.value || '')}></IonInput>
                    </IonItem>
                </IonList>
                <IonList>
                    <IonItem>
                        <IonLabel>連接伺服器</IonLabel>
                        <IonSelect
                            interface="alert"
                            value={serverSelected} placeholder="請選擇其中一個"
                            onIonChange={e => setServerSelected(e.detail.value || '' )}>
                            <IonSelectOption value="logistics1">Logistics 1</IonSelectOption>
                            <IonSelectOption value="logistics2">Logistics 2</IonSelectOption>
                            <IonSelectOption value="logistics3">Logistics 3</IonSelectOption>
                            <IonSelectOption value="logistics4">Logistics 4</IonSelectOption>
                        </IonSelect>
                    </IonItem>
                </IonList>
                <div className={styles.infoButtonDiv}>
                    <div className={styles.infoButton}>
                        <IonButton className={styles.buttonGroup1} onClick={submit}>登入</IonButton>
                    </div>
                    <div className={styles.infoButton}>
                        <IonButton className={styles.buttonGroup2} routerLink={routes.register}>前往註冊</IonButton>
                    </div>
                </div>
                <div>
                <APIResult result={result} />
                {/* {console.log('result: ', result)} */}
                </div>
            </div>
        </div>
    )
}