import { IonButton, IonContent, IonInput, IonItem, IonLabel, IonList, IonText, useIonAlert } from "@ionic/react"
import { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { post } from "../api"
import { RootState } from "../redux/state"

type FormState = {
    oldPassword: string
    password: string
    confirm_password: string
}

export function ChangePw(){
    const userInfo = useSelector((state: RootState) => {
        return {
            username: state.auth.user?.username,
            company: state.accountInfo.companyName,
            serverSelected: state.auth.serverSelected
        }
    })

    const dispatch = useDispatch()
    const [presentAlert] = useIonAlert()

    const [formState, setFormState] = useState<FormState>({
        oldPassword: '',
        password: '',
        confirm_password: '',
    })

    const messages = {
        oldPassword: formState.oldPassword === formState.password? '新密碼與舊密碼相同，請重新輸入' : '',
        password: formState.password.length < 8 ? '密碼須最少8個字元' : '',
        confirm_password: formState.confirm_password !== formState.password ? '2次輸入的密碼並不相同，請重新輸入' : '',
    }

    const submit = async () =>{
     //   console.log('formState: ', formState);
        const invalidMessage = Object.values(messages).find(message => message.length > 0)
        if (invalidMessage) {
            presentAlert(invalidMessage, [{ text: '確定', role: "cancel" }])
            return
        }
        if(userInfo.serverSelected){
        let changePW = await post('/changePW', userInfo.serverSelected , {
            username: userInfo.username, 
            company: userInfo.company ,
            oldPassword: formState.oldPassword, 
            newPassword: formState.password})
 //       console.log(changePW);
        if(changePW.message === 'password updates'){
            presentAlert('已成功更改密碼！', [{ text: '確定', role: "cancel" }])
            setFormState({
                oldPassword: '',
                password: '',
                confirm_password: '',
            })
        }
        if(changePW.error === 'Error: Wrong username or password'){
            presentAlert('密碼錯誤，請重新輸入！', [{ text: '確定', role: "cancel" }])
        }        
    }
        
    }

    function getHintField() {
        for (let [key, value] of Object.entries(formState)) {
            const field = key as keyof FormState
            const validationMessage = messages[field]
            if (!value || validationMessage) {
                return field
            }
        }
    }
    let hintField = getHintField()

    const renderField = (props: {
        field: keyof FormState
        inputType: 'text' | 'password'
        showName: '舊密碼' | '密碼' | '確認密碼'
    }) => {
        const { field, inputType, showName } = props
        const value = formState[field]
        const validationMessage = messages[field]

        return (
            <>
                <IonItem>
                    <IonLabel
                        position="floating"
                        color={
                            !value ? (hintField === field ? 'primary' : undefined) : validationMessage ? 'danger' : undefined}>
                        {showName}
                    </IonLabel>
                    <IonInput
                        type={inputType}
                        value={value}
                        onIonChange={e => setFormState({ ...formState, [field]: e.detail.value || '' })}>
                    </IonInput>
                </IonItem>
                <IonText color={
                    !value ? 'light' : 'danger'
                }>{validationMessage}</IonText>
            </>
        )
    }

    return (
        <>
            <IonContent className="ion-padding">
                <IonList>
                    {renderField({ field: 'oldPassword', inputType: "password", showName: '舊密碼' })}
                    {renderField({ field: 'password', inputType: "password", showName: '密碼' })}
                    {renderField({ field: 'confirm_password', inputType: "password", showName: '確認密碼' })}
                </IonList>
                <IonButton style={{"--background": "#112A46","--border-radius": "2em","marginTop":"2rem"}} expand="block" onClick={submit}>更改密碼</IonButton>
            </IonContent>
        </>
    )
}


