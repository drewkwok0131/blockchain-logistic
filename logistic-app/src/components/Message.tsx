import { IonButton, IonItem, IonList, IonIcon, useIonAlert, IonAlert } from "@ionic/react"
import { copy, trash } from 'ionicons/icons';
import { SetStateAction, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { post } from "../api";
import { RootState } from "../redux/state";
import styles from './Message.module.css'


type Message = {
    id: number,
    text: string,
    transactionNumber: string
    is_show: boolean
}

export function Message() {
    const [message, setMessage] = useState([])
    const [presentAlert] = useIonAlert()
    const userInfo = useSelector((state: RootState) => {
        return {
            username: state.auth.user?.username,
            serverSelected: state.auth.serverSelected!,
        }
    })
    console.log(message);

    useEffect(() => {
        const fetchData = async () => {
            let result = await post('/message', userInfo.serverSelected, userInfo)
            console.log(result);

            setMessage(result)
        }
        fetchData().catch(console.error)
    }, [])

    const hiddenMessage = async (id: string) => {
        let result = await post('/message/hidden', userInfo.serverSelected, { id: id })
        console.log('hidden result: ', result);

        if (result.message === 'fail to hide message') {
            presentAlert('你未能刪除訊息，請與管理員聯絡', [{ text: '確定', role: "cancel" }])
            return
        }
        let newMessageList: SetStateAction<never[]> = []
        for (let messageItem of message) {
            if (messageItem['id'] !== parseInt(id)) {
                newMessageList.push(messageItem)
            }
        }
        setMessage(newMessageList)
    }

    return (
        <div className={styles.resultOrder}>
        {message.map((item: Message) => (
                <div>
                    <IonItem hidden={!item.is_show} key={item.id}>
                        <IonList>
                            <p>Message:</p>
                            <p>{item.text} </p>
                            <IonButton
                                style={{ '--background': '#112A46' }}
                                onClick={() => { navigator.clipboard.writeText(item.transactionNumber) }}>
                                <IonIcon icon={copy} />
                            </IonButton>
                            <IonButton
                                style={{ '--background': '#dd0000' }}
                                onClick={() => { hiddenMessage(item.id.toString()) }}>
                                <IonIcon icon={trash} />
                            </IonButton>
                        </IonList>
                    </IonItem>
                </div>
            ))}
        </div>

    )
}