import { IonButton, IonButtons, IonContent, IonHeader, IonModal, IonTitle, IonToolbar } from "@ionic/react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/state";
import styles from "../pages/AccountPage.module.css";
import { useEffect, useState } from "react";
import { Message } from "./Message";
import { ChangePw } from "./ChangePw";
import { accountInfoThunk } from "../redux/accountInfo/thunk";
import { logoutThunk } from "../redux/auth/thunk";
import { routes } from "../routes";

interface Props {
    router: HTMLIonRouterOutletElement | null;
}


export function Account(prop: Props) {
    const userInfo = useSelector((state: RootState) => {
        return {
            username: state.auth.user?.username,
            email: state.accountInfo.email,
            company: state.accountInfo.companyName,
            serverSelected: state.auth.serverSelected,
            role: state.auth.user?.role
        }
    })
    const [isOpenModalMessage, setIsOpenModalMessage] = useState(false)
    const [isOpenModalPw, setIsOpenModalPw] = useState(false)
    const dispatch = useDispatch()

    useEffect(()=>{
        dispatch(accountInfoThunk({ username: userInfo!.username! ,serverName: userInfo.serverSelected!}))
    },[])

    const logout = () => {
        dispatch(logoutThunk())

    }

    return (
        <>
            <div className={styles.infoContainer}>
                <div className={styles.infoDiv}>
                    <p>用戶名稱： {userInfo ? userInfo.username : 'Please go to login'}</p>
                </div>
                <div className={styles.infoDiv}>
                    <p>公司名稱：{userInfo.company ? userInfo.company : 'Please go to login'}</p>
                </div>
                <div className={styles.infoDiv}>
                    <p>電郵地址： {userInfo.email ? userInfo.email : 'Please go to login'}</p>
                </div>
            </div>
            <div className={styles.infoButtonDiv}>
                <div hidden={userInfo.role === 'staff'} className={styles.infoButton}>
                    <IonButton className={styles.buttonGroup1} onClick={() => { setIsOpenModalMessage(true) }}>訊息</IonButton>
                </div>
                <div className={styles.infoButton}>
                    <IonButton className={styles.buttonGroup1} onClick={() => { setIsOpenModalPw(true) }}>更改密碼</IonButton>
                </div>
                <div className={styles.infoButton}>
                    <IonButton className={styles.logoutButton} routerLink={routes.welcome} onClick={logout}>登出</IonButton>
                </div>
            </div>

            {/* read message */}

            <IonModal isOpen={isOpenModalMessage}
                swipeToClose={true}
                presentingElement={prop.router || undefined}
                backdropDismiss={true}>
                <IonHeader translucent>
                    <IonToolbar style={{ '--background': "#FCF0E7" , "font-family" :"'Noto Sans TC', sans-serif"}}>
                        <IonTitle>訊息</IonTitle>
                        <IonButtons slot="end">
                            <IonButton onClick={() => setIsOpenModalMessage(false)}>關閉</IonButton>
                        </IonButtons>
                    </IonToolbar>
                </IonHeader>
                <IonContent>
                    <Message></Message>
                </IonContent>
            </IonModal>

            {/* amend pw */}

            <IonModal isOpen={isOpenModalPw}
                swipeToClose={true}
                presentingElement={prop.router || undefined}
                backdropDismiss={true}>
                <IonHeader translucent>
                    <IonToolbar style={{ '--background': "#FCF0E7" , "font-family" :"'Noto Sans TC', sans-serif" }}>
                        <IonTitle>更改密碼</IonTitle>
                        <IonButtons slot="end">
                            <IonButton onClick={() => setIsOpenModalPw(false)}>關閉</IonButton>
                        </IonButtons>
                    </IonToolbar>
                </IonHeader>
                <IonContent>
                    <ChangePw></ChangePw>
                </IonContent>
            </IonModal>
        </>
    )
}