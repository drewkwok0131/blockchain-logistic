import { IonText, useIonAlert } from "@ionic/react";
import { APIResultType } from "../redux/auth/state";

export function APIResult(props: {result: APIResultType}){
    const [presentAlert] = useIonAlert()
    
    const {result} = props
    return (
    <>
        {result.type !== 'fail' ? null : (
    <p>
        <IonText color={result.type === 'fail' ? 'danger' : 'primary'}>
            {
            result.message === 'Error: Wrong username or password'? presentAlert(result.message = '用戶名稱或密碼錯誤') : 
            result.message === 'Error: username not found'? presentAlert(result.message = '此用戶未有登記') : 
            result.message === 'Error: duplicated username'? presentAlert(result.message = '此用戶名稱已被其他人使用') : 
            result.message === 'Error: duplicated email'? presentAlert(result.message = '此電郵地址已有用戶登記') : 
            result.message === 'Error: cannot find this company'? presentAlert(result.message = '公司名稱輸入錯誤') : 
            result.message                      
            }
        </IonText>
    </p>
        )}
    </>
    )
}
